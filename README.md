# boco-backend

## Project
This is the back end repository for group 3 in the SCRUM-project in the subject IDATT2106 at NTNU. The project took place from 19.04.2022 to 16.05.2022.

Here you can find the code used in conjunction with the [front end](https://gitlab.stud.idi.ntnu.no/idatt2106-v21-03/boco-frontend) to serve up the BOCO service.

## BOCO
BOCO is a fictional business concept where users may rent out items they own and/or rent others' items on a public marketplace.

The service provides a way for users to create, edit and delete items for rental, rent others' items and message others users.

To learn more about BOCO, please refer to the [vision document](https://idatt2106-v21-03.pages.stud.idi.ntnu.no/vision-document/).

## Additional information
More information about the project in its entirety may be found on our [Wiki](https://gitlab.stud.idi.ntnu.no/idatt2106-v21-03/boco-backend/-/wikis/home).

## Project setup
First you will need a local `.env` file in the root folder of the project. This is done to prevent accidentally sharing crucial, private business information such as passwords and other database connection information. Your environment file should like the following:

```
DB_URL=jdbc:mysql://dev.boco.gq/fs_idatt2106_1_team3
DB_USERNAME=root
DB_PASSWORD=a@]ed#VR
ADMIN_EMAIL=admin@example.com
ADMIN_PASSWORD=admin
```

Description of environment variables:
- `DB_URL`: JDBC URL for the database to use (e.g. `jdbc:mysql://localhost:3306/my_db`)
- `DB_USERNAME`: Username used to log in to the database
- `DB_PASSWORD`: Cleartext password used to log into the database
- `ADMIN_EMAIL`: Desired email for an admin user (does not have to be a real email)
- `ADMIN_PASSWORD`: Desired password for an admin user


## Run locally
With the environment file in place, you may run the following command to serve up the back end:
```
mvn spring-boot:run
```

## Run unit tests
```
mvn test
```