package edu.ntnu.idatt2106.bocobackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(
	exclude = {
		SecurityAutoConfiguration.class
	}
)
public class BocoBackendApplication {
	public static void main(String[] args) {
		SpringApplication.run(BocoBackendApplication.class, args);
	}

}
