package edu.ntnu.idatt2106.bocobackend;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    
    private final String allowedOrigin;

    public WebSocketConfig(
        @Value("${cors.allowed-origin}") String allowedOrigin
    ) {
        this.allowedOrigin = allowedOrigin;
    }

    @Override
        public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/chats");
        config.setApplicationDestinationPrefixes("/chats");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/ws")
            .setAllowedOriginPatterns(this.allowedOrigin)
            .withSockJS();
    }
}
