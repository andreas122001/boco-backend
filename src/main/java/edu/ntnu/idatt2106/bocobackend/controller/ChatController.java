package edu.ntnu.idatt2106.bocobackend.controller;

import java.time.Instant;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import edu.ntnu.idatt2106.bocobackend.dto.ChatDTO;
import edu.ntnu.idatt2106.bocobackend.dto.ChatMessageDTO;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.service.ChatService;
import edu.ntnu.idatt2106.bocobackend.util.Actor;

@RestController
@RequestMapping("/chats")
public class ChatController {
    
    Logger logger = LoggerFactory.getLogger(ChatController.class);

    private final ChatService chatService;
    
    public ChatController(@Autowired ChatService chatService) {
        this.chatService = chatService;
    }

    /**
     * Creates and sends a message in a chat
     * 
     * @param uuid the uuid of the chat the message should be sent to
     * @param message
     * @return message 
     */
    @MessageMapping("/{uuid}/send-message")
    @SendTo("/chats/{uuid}/messages")
    public ChatMessageDTO sendMessage(
        @DestinationVariable String uuid,
        ChatMessageDTO message
    ) {
        logger.info("Received websocket message from " + message.getSenderEmail() + " on chat with UUID " + uuid);

        User sender = Actor.getCurrent().getUser();

        this.chatService.createChatMessage(uuid, message, sender);

        return message;
    }

    /**
     * Gets all chats in which the user is a participant
     * @return all chats in which the user is a participant
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ChatDTO> getChats() {
        User user = Actor.getCurrent().getUser();

        return this.chatService.getChats(user);
    }

    @GetMapping("/{itemId}")
    @ResponseStatus(HttpStatus.OK)
    public ChatDTO getChat(@PathVariable long itemId) {
        User user = Actor.getCurrent().getUser();

        return this.chatService.getChat(itemId, user);
    }

    /**
     * Creates a chat 
     * @param chatDto 
     * @return the created chat
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ChatDTO create(@RequestBody ChatDTO chatDto) {
        User initiator = Actor.getCurrent().getUser();

        return this.chatService.createChat(chatDto, initiator);
    }

    /**
     * Gets all messages in a given chat by the chat's id
     * @param uuid the uuid of the chat to fetch messages from
     * @return all messages in the given chat
     */
    @GetMapping("/{uuid}/messages")
    @ResponseStatus(HttpStatus.OK)
    public List<ChatMessageDTO> getMessages(@PathVariable String uuid) {
        User user = Actor.getCurrent().getUser();

        return this.chatService.getMessages(uuid, user);
    }

    /**
     * Sends a lease notice in a given chat.
     * @param uuid the uuid of the chat to send the notice in.
     * @param leaseNotice the notice to send.
     * @return the newly created message.
     */
    @PostMapping("/{uuid}")
    @ResponseStatus(HttpStatus.CREATED)
    public ChatMessageDTO sendLeaseNotice(@PathVariable String uuid, @RequestBody String leaseNotice) {
        User user = Actor.getCurrent().getUser();
        ChatMessageDTO messageDto = new ChatMessageDTO(Instant.now(), leaseNotice, user.getEmail());

        return this.chatService.createChatMessage(uuid, messageDto, user);
    }
}
