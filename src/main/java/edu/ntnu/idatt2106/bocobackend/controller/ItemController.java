package edu.ntnu.idatt2106.bocobackend.controller;
import org.springframework.http.MediaType;
import edu.ntnu.idatt2106.bocobackend.dto.ItemDTO;
import edu.ntnu.idatt2106.bocobackend.dto.ItemImageDTO;
import edu.ntnu.idatt2106.bocobackend.dto.LeasePeriodDTO;
import edu.ntnu.idatt2106.bocobackend.dto.ItemsPageDTO;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.service.ItemService;
import edu.ntnu.idatt2106.bocobackend.service.LeaseService;
import edu.ntnu.idatt2106.bocobackend.util.Actor;
import edu.ntnu.idatt2106.bocobackend.util.ItemCategory;
import edu.ntnu.idatt2106.bocobackend.util.PriceUnit;

import edu.ntnu.idatt2106.bocobackend.util.PaginationConstants;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/items")
public class ItemController {


    private static final Logger logger = LogManager.getLogger(ItemController.class);

    private final ItemService itemService;
    private final LeaseService leaseService;

    public ItemController(@Autowired ItemService itemService,
     @Autowired LeaseService leaseService) {
        this.itemService = itemService;
        this.leaseService = leaseService;
    }

    /**
     * Creates an item for the authenticated user.
     *
     * @param item to create.
     * @return the created item.
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ItemDTO create(@RequestBody ItemDTO item) {
        logger.info("Create item {}", item);
        User user = Actor.getCurrent().getUser();
        logger.debug("User: {}", user);

        return itemService.create(item, user);
    }

    /**
     * Gets and paginates items.
     * Method can also sort, filter and search individually or combined by choice,
     * and be implemented on a single user's items instead of all items.
     * 
     * @param page the number of pages to paginate on, default 0
     * @param limit the number of items per page, default Integer.MAX
     * @param sort the value to sort the items by, default is id
     * @param sortDir used to select between ASC and DESC sorting, default is ASC
     * @param search the value to search the items by, default is ""
     * @param category the category to filter the items by
     * @param userEmail the user's email, used to implement the method on a single user's items, as opposed to all items 
     * 
     * @return paginated items and response data
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ItemsPageDTO getItems(
        @RequestParam(value = "page", defaultValue = PaginationConstants.DEFAULT_PAGE_NUMBER , required = false) int page,
        @RequestParam(value = "limit", defaultValue = PaginationConstants.DEFAULT_PAGE_SIZE, required = false) int limit,
        @RequestParam(value = "sort", defaultValue = PaginationConstants.DEFAULT_SORT_BY, required = false) String sort,
        @RequestParam(value = "sortDir", defaultValue = PaginationConstants.DEFAULT_SORT_DIRECTION, required = false) String sortDir,
        @RequestParam(value = "search", required = false) String search,
        @RequestParam(value = "email", required = false) String email,
        @RequestParam(value = "category", required = false) String category
    ) {
        return itemService.getItems(page, limit, sort, sortDir, search, email, category);
    }

    /**
     * Gets an item by id.
     *
     * @param id of the item.
     * @return the item.
     */
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ItemDTO getItemById(@PathVariable long id) {
        logger.info("Get item by id {}", id);
        return itemService.getById(id);
    }

    /**
     * Deletes an item by id.
     *
     * @param id             of the item.
     * @return the deleted item.
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ItemDTO deleteById(@PathVariable long id) {
        User user = Actor.getCurrent().getUser();
        logger.info("Delete item by id {} of user {}", id, user);

        return itemService.deleteById(user, id);
    }

    /**
     * Updates an item.
     *
     * @param item           the updated item.
     * @return the updated item.
     */
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ItemDTO updateItem(@PathVariable long id,
            @RequestBody ItemDTO item) {
        User user = Actor.getCurrent().getUser();
        logger.info("Update item {} of user {}", item, user);

        return itemService.update(user, id, item);
    }

    /**
     * Returns all price units.
     *
     * @return price units.
     */
    @GetMapping("/priceUnits")
    @ResponseStatus(HttpStatus.OK)
    public List<PriceUnit> priceUnits() {
        return Arrays.asList(PriceUnit.values());
    }

    /**
     * Returns all categories.
     *
     * @return categories.
     */
    @GetMapping("/categories")
    @ResponseStatus(HttpStatus.OK)
    public List<ItemCategory> categories() {
        return Arrays.asList(ItemCategory.values());
    }

    @GetMapping("/{itemId}/images/{imageId}")
    @ResponseStatus(HttpStatus.OK)
    public ItemImageDTO getItemImageById(@PathVariable long itemId,
            @PathVariable long imageId) {
        logger.info("Get imageId {} of itemId {}", imageId, itemId);

        return itemService.getImageById(imageId);
    }

    /**
     * Adds an image to an item. Checks if user is the owner of item.
     *
     * @param itemId         of item.
     * @param image          to add.
     */
    @PostMapping("/{itemId}/images")
    @ResponseStatus(HttpStatus.CREATED)
    public ItemImageDTO addImageByItemId(@PathVariable long itemId,
            @RequestBody ItemImageDTO image) {
        logger.info("Add image to item with id {}", itemId);
        User user = Actor.getCurrent().getUser();

        return itemService.addImageToItem(user, image, itemId);
    }

    /**
     * Removes an image from an item. Check if user is owner of item.
     *
     * @param itemId         id of item.
     * @param imageId        id of image.
     */
    @DeleteMapping("/{itemId}/images/{imageId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteImageByItemId(
            @PathVariable long itemId,
            @PathVariable long imageId) {
        logger.info("Remove image from item with id {}", itemId);
        User user = Actor.getCurrent().getUser();

        itemService.removeImageFromItem(user, imageId, itemId);
    }

     /**
     * Gets all leases by item id
     * @param itemId
     * @return LeasPeriodDTO
     */
    @GetMapping( value = "/{itemId}/leases",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<LeasePeriodDTO> getLeasesByItemId(@PathVariable long itemId){
        return leaseService.getAllLeasesByItemID(itemId);
    }
}
