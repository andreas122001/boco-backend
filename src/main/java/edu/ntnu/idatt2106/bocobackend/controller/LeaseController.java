package edu.ntnu.idatt2106.bocobackend.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import edu.ntnu.idatt2106.bocobackend.dto.LeaseDTO;
import edu.ntnu.idatt2106.bocobackend.model.Username;
import edu.ntnu.idatt2106.bocobackend.service.LeaseService;
import edu.ntnu.idatt2106.bocobackend.util.Actor;

@RestController
@RequestMapping(value = "/leases")
public class LeaseController {

    private final LeaseService leaseService;

    public LeaseController(@Autowired LeaseService leaseService) {
        this.leaseService = leaseService;
    }

    /**
     * Creates a new lease.
     * 
     * @param leaseDTO DTO of a lease object to be created.
     * @return The lease that was created.
     */
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(code = HttpStatus.CREATED)
    public LeaseDTO create(
        @RequestParam(defaultValue = Username.SELF) String username,
        @RequestBody LeaseDTO leaseDTO
    ) {
        return this.leaseService.create(Actor.getCurrent(), username, leaseDTO);
    }
    
    /**
     * Gets all leases tied to a user.
     * 
     * @param authentication Authentication context, used to retrieve user.
     * @return All leases tied to user in auth context.
     */
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<LeaseDTO> get(
        @RequestParam(defaultValue = Username.SELF) String username
    ) {
        return this.leaseService.getLeasesByUsername(Actor.getCurrent(), username);
    }
    /**
     * Gets a lease by its id.
     * 
     * @param id The id of the lease to get.
     * @return The lease with the specific id.
     */
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public LeaseDTO getById(@PathVariable String id) {
        return this.leaseService.getById(Actor.getCurrent(), id);
    }

    /**
     * Deletes a lease by its id.
     * 
     * @param id The id of the lease to delete.
     */
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteById(@PathVariable String id) {
        this.leaseService.deleteById(Actor.getCurrent(), id);
    }
}
