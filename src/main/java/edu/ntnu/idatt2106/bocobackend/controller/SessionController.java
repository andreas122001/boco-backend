package edu.ntnu.idatt2106.bocobackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import edu.ntnu.idatt2106.bocobackend.dto.SessionDTO;
import edu.ntnu.idatt2106.bocobackend.model.UserCredentials;
import edu.ntnu.idatt2106.bocobackend.service.SessionService;


/**
 * SessionController handles requests for the SessionService.
 */
@RestController
@RequestMapping(value = "/sessions")
public class SessionController {
    private final SessionService sessionService;

    public SessionController(@Autowired SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * Creates a new session.
     * @param credentials The user credentials.
     * @return The session that has been created.
     */
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(code = HttpStatus.CREATED)
    public SessionDTO create(@RequestBody UserCredentials credentials) {
        return this.sessionService.create(credentials);
    }

    /**
     * Deletes a session.
     * @param token The session token.
     */
    @DeleteMapping(value = "/{token}")
    public void delete(@PathVariable String token) {
        this.sessionService.delete(token);
    }
}
