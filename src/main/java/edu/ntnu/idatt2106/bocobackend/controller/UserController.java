package edu.ntnu.idatt2106.bocobackend.controller;

import edu.ntnu.idatt2106.bocobackend.dto.ChangePasswordDTO;
import edu.ntnu.idatt2106.bocobackend.dto.UserDTO;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.model.Username;
import edu.ntnu.idatt2106.bocobackend.service.UserService;
import edu.ntnu.idatt2106.bocobackend.util.Actor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    private static final Logger logger = LogManager.getLogger(UserController.class);

    private final UserService userService;

    public UserController(@Autowired UserService userService) {
        this.userService = userService;
    }

    /**
     * Registers a new user.
     * @param user to register.
     */
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public User register(@RequestBody UserDTO user) {
        logger.info("Register new user: {}", user);
        return userService.create(user);
    }

    /**
     * Gets the current user from session.
     * @return the user.
     */
    @GetMapping("/" + Username.SELF)
    @ResponseStatus(HttpStatus.OK)
    public User getSelf() {
        User user = Actor.getCurrent().getUser();
        logger.info("Get user {}", user);
        return user;
    }

    @GetMapping(value = "/id/{id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public UserDTO getById(@PathVariable long id) {
        logger.info("Get user by id {}", id);
        return userService.getProfileById(id);
    }

    /**
     * Finds a user by email and returns it.
     * @param email of user.
     * @return the user.
     */
    @GetMapping(value = "/{email}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public UserDTO getByEmail(@PathVariable String email) {
        logger.info("Get user by email: {}", email);
        return userService.getProfileByEmail(email);
    }

    /**
     * Deletes a user by email. User is validated in service.
     * @param email of user.
     * @return the deleted user.
     */
    @DeleteMapping("/{email}")
    @ResponseStatus(HttpStatus.OK)
    public User deleteByEmail(@PathVariable String email) {
        Actor actor = Actor.getCurrent();
        logger.info("User {} requested deletion of user with email: {}", actor.getUser(), email);
        return userService.deleteByUsername(actor, email);
    }

    /**
     * Updated password of a user using the session.
     * @param username of user.
     * @param changePasswordDTO dto of password change.
     */
    @PutMapping("/{username}/password")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void setPassword(
        @PathVariable String username,
        @RequestBody ChangePasswordDTO changePasswordDTO
    ) {
        Actor actor = Actor.getCurrent();
        logger.info("Update password for user: {}", actor.getUser());
        userService.setPasswordByUsername(actor, username, changePasswordDTO);
    }

    /**
     * Updates a user. Validation in service.
     * @param username of user.
     * @param user updated user.
     * @return the updated user.
     */
    @PutMapping("/{username}")
    @ResponseStatus(HttpStatus.OK)
    public User updateUser(@PathVariable String username,
                           @RequestBody UserDTO user) {
        logger.info("Update user {}", user);
        return userService.updateUserByUsername(
            Actor.getCurrent(),
            username,
            user
        );
    }

}
