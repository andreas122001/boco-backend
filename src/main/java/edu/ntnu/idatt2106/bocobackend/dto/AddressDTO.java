package edu.ntnu.idatt2106.bocobackend.dto;

/**
 * A DTO for the model-class Address implemented to
 * ease interaction(s) between frontend and backend
 */
public class AddressDTO {
    private String street;

    private String postalCode;

    public AddressDTO() {}
    
    public AddressDTO(String street, String postalCode) {
        this.street = street;
        this.postalCode = postalCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
}
