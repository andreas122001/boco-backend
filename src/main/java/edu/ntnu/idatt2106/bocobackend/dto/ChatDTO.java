package edu.ntnu.idatt2106.bocobackend.dto;

import java.util.UUID;

/**
 * A DTO for the model-class Chat, implemented to
 * ease interaction(s) between frontend and backend
 */
public class ChatDTO {
    private UUID uuid;
    private long itemId;
    private String lessorEmail;
    private String initiatorEmail;

    public ChatDTO() {}
    
    public ChatDTO(long itemId, String lessorEmail, String initiatorEmail) {
        this.itemId = itemId;
        this.lessorEmail = lessorEmail;
        this.initiatorEmail = initiatorEmail;
    }

    public String getLessorEmail() {
        return lessorEmail;
    }

    public void setLessorEmail(String lessorEmail) {
        this.lessorEmail = lessorEmail;
    }

    public String getInitiatorEmail() {
        return initiatorEmail;
    }

    public void setInitiatorEmail(String initiatorEmail) {
        this.initiatorEmail = initiatorEmail;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }
}
