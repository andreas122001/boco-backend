package edu.ntnu.idatt2106.bocobackend.dto;

import java.time.Instant;

/**
 * A DTO for the model-class ChatMessage, implemented to
 * ease interaction(s) between frontend and backend
 */
public class ChatMessageDTO {
    private Instant timestamp;
    private String content;
    private String senderEmail;

    public ChatMessageDTO() {}
    
    public ChatMessageDTO(Instant timestamp, String content, String senderEmail) {
        this.timestamp = timestamp;
        this.content = content;
        this.senderEmail = senderEmail;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String message) {
        this.content = message;
    }

    public String getSenderEmail() {
        return senderEmail;
    }
    
    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }
}