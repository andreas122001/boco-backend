package edu.ntnu.idatt2106.bocobackend.dto;

import edu.ntnu.idatt2106.bocobackend.util.ItemCategory;
import edu.ntnu.idatt2106.bocobackend.util.PriceUnit;

import java.time.Instant;
import java.util.List;

/**
 * A DTO for the model-class Item, implemented to
 * ease interaction(s) between frontend and backend
 */
public class ItemDTO {
    private Long id;
    private String title;
    private int price;
    private PriceUnit priceUnit;
    private ItemCategory category;
    private Instant startTime;
    private Instant endTime;
    private String description;
    private String userEmail;
    private List<Long> imageIds;
    private AddressDTO address;

    public ItemDTO() {};

    public ItemDTO(
        Long id,
        String title, 
        int price,
        PriceUnit priceUnit, 
        ItemCategory itemCategory,
        Instant startTime,
        Instant endTime,
        String description,
        String userEmail,
        List<Long> imageIds
    ) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.priceUnit = priceUnit;
        this.category = itemCategory;
        this.startTime = startTime;
        this.endTime = endTime;
        this.description = description;
        this.userEmail = userEmail;
        this.imageIds = imageIds;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public PriceUnit getPriceUnit() {
        return priceUnit;
    }

    public void setPriceUnit(PriceUnit priceUnit) {
        this.priceUnit = priceUnit;
    }

    public ItemCategory getCategory() {
        return category;
    }

    public void setCategory(ItemCategory category) {
        this.category = category;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public List<Long> getImageIds() {
        return imageIds;
    }

    public void setImageIds(List<Long> imageIds) {
        this.imageIds = imageIds;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }
}
