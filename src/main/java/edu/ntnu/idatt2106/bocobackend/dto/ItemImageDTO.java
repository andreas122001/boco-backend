package edu.ntnu.idatt2106.bocobackend.dto;

/**
 * A DTO for the model-class ItemImage, implemented to
 * ease interaction(s) between frontend and backend
 */
public class ItemImageDTO {
    private Long id;
    private String description;
    private String data;
    private Long itemId;

    public ItemImageDTO() {}
    
    public ItemImageDTO(Long id, String description, String data, Long itemId) {
        this.id = id;
        this.description = description;
        this.data = data;
        this.itemId = itemId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getData() {
        return this.data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Long getItemId() {
        return this.itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }
}
