package edu.ntnu.idatt2106.bocobackend.dto;

import java.util.List;

/**
 * ItemsPage DTO to used to implement pagination
 */
public class ItemsPageDTO {
    private List<ItemDTO> items;
    private int pageNo;
    private int pageSize;
    private long totalElements;
    private int totalPages;
    private boolean last;

    public ItemsPageDTO() {}

    public List<ItemDTO> getContent() {
        return items;
    }

    public int getPageNo() {
        return pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public boolean getLast() {
        return last;
    }

    public void setContent(List<ItemDTO> items) {
        this.items = items;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public void setLast(boolean last) {
        this.last = last;
    }
}
