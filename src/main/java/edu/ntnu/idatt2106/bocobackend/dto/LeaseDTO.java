package edu.ntnu.idatt2106.bocobackend.dto;

import java.time.Instant;

/**
 * A DTO for the model-class Lease, implemented to
 * ease interaction(s) between frontend and backend
 */
public class LeaseDTO {
    private String id;
    private String lesseeEmail;
    private long itemId;
    private Instant startTime;
    private Instant endTime;

    public LeaseDTO() {}

    public LeaseDTO(String id, String lesseeEmail, long itemId, Instant startTime, Instant endTime) {
        this.id = id;
        this.lesseeEmail = lesseeEmail;
        this.itemId = itemId;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLesseeEmail() {
        return this.lesseeEmail;
    }

    public void setLesseeEmail(String lesseeEmail) {
        this.lesseeEmail = lesseeEmail;
    }

    public long getItemId() {
        return this.itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public Instant getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }
}
