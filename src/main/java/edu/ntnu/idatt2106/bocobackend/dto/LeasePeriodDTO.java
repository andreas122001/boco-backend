package edu.ntnu.idatt2106.bocobackend.dto;

import java.time.Instant;

/**
 * LeasePeriod DTO to send only start and endtime for a lease
 */
public class LeasePeriodDTO {
    private Instant startTime;
    private Instant endTime;

    public LeasePeriodDTO() {}
    
    public LeasePeriodDTO(Instant startTime, Instant endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }


    public Instant getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

}
