package edu.ntnu.idatt2106.bocobackend.dto;

/**
 * A DTO for the model-class Session, implemented to
 * ease interaction(s) between frontend and backend
 */
public class SessionDTO {
    private String token;

    public SessionDTO() {}
    
    public SessionDTO(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
