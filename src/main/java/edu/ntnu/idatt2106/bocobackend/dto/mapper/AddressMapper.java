package edu.ntnu.idatt2106.bocobackend.dto.mapper;

import org.mapstruct.Mapper;

import edu.ntnu.idatt2106.bocobackend.dto.AddressDTO;
import edu.ntnu.idatt2106.bocobackend.model.Address;

/**
 * Class to map between an AddressDTO and Address.
 * See AddressMapperImpl.java for implemented methods.
 */
@Mapper(componentModel = "spring")
public abstract class AddressMapper {
    public abstract AddressDTO toDTO(Address dto);
    public abstract Address fromDTO(AddressDTO dto);
}
