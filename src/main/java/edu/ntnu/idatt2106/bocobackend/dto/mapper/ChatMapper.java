package edu.ntnu.idatt2106.bocobackend.dto.mapper;

import java.util.Collection;
import java.util.List;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import edu.ntnu.idatt2106.bocobackend.dto.ChatDTO;
import edu.ntnu.idatt2106.bocobackend.model.Chat;
import edu.ntnu.idatt2106.bocobackend.model.ChatPrimaryKey;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.service.ItemService;

/**
 * Class to map between a ChatDTO and Chat.
 * See ChatMapperImpl.java for implemented methods.
 */
@Mapper(
    componentModel = "spring"
)
public abstract class ChatMapper {
    @Autowired
    ItemService itemService;

    @Named("createPrimaryKey")
    ChatPrimaryKey createPrimaryKey(long itemId, @Context User user) {
        return new ChatPrimaryKey(itemService.getItemById(itemId), user);
    }

    @Mappings({
        @Mapping(target = "lessorEmail", source = "primaryKey.item.user.email"),
        @Mapping(target = "initiatorEmail", source = "primaryKey.initiator.email"),
        @Mapping(target = "itemId", source = "primaryKey.item.id")
    })
    public abstract ChatDTO toDTO(Chat chat);
    public abstract List<ChatDTO> toDTOs(Collection<Chat> chats);

    @Mappings({
        @Mapping(target = "uuid", ignore = true),
        @Mapping(target = "primaryKey", source = "dto.itemId", qualifiedByName = "createPrimaryKey")
    })
    public abstract Chat fromDTO(ChatDTO dto, @Context User initiator);
}
