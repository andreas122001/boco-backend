package edu.ntnu.idatt2106.bocobackend.dto.mapper;

import java.time.Instant;
import java.util.Collection;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import edu.ntnu.idatt2106.bocobackend.dto.ChatMessageDTO;
import edu.ntnu.idatt2106.bocobackend.model.Chat;
import edu.ntnu.idatt2106.bocobackend.model.ChatMessage;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.service.ItemService;

/**
 * Class to map between a ChatMessageDTO and ChatMessage.
 * See ChatMessageMapperImpl.java for implemented methods.
 */
@Mapper(
    componentModel = "spring"
)
public abstract class ChatMessageMapper {
    @Autowired
    ItemService itemService;

    @Named("timestamp")
    Instant timestamp(ChatMessageDTO dto) {
        return Instant.now();
    }

    @Mappings({
        @Mapping(target = "senderEmail", source = "sender.email")
    })
    public abstract ChatMessageDTO toDTO(ChatMessage chatMessage);
    public abstract List<ChatMessageDTO> toDTOs(Collection<ChatMessage> chatMessage);

    @Mappings({
        @Mapping(target = "timestamp", source = "dto", qualifiedByName = "timestamp"),
    })
    public abstract ChatMessage fromDTO(
        ChatMessageDTO dto, 
        Chat chat, 
        User sender
    );
}
