package edu.ntnu.idatt2106.bocobackend.dto.mapper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.tomcat.util.codec.binary.Base64;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

import edu.ntnu.idatt2106.bocobackend.dto.ItemImageDTO;
import edu.ntnu.idatt2106.bocobackend.model.Item;
import edu.ntnu.idatt2106.bocobackend.model.ItemImage;

/**
 * Class to map between a ItemImageDTO and ItemImage.
 * See ItemImageMapperImpl.java for implemented methods.
 */
@Mapper(componentModel = "spring")
public abstract class ItemImageMapper {
    private static final Pattern DATA_PATTERN = Pattern.compile(
        "^data:(image/png|image/jpeg|image/webp|image/gif);base64,(.+)$"
    );

    private static Matcher getMatcherForDataUrl(String dataUrl) {
        Matcher matcher = DATA_PATTERN.matcher(dataUrl);
        matcher.find();
        return matcher;
    }

    @Named("dataUrlToMimeType")
    String dataUrlToMimeType(String dataUrl) {
        return getMatcherForDataUrl(dataUrl).group(1);
    }

    @Named("dataUrlToData")
    byte[] dataUrlToData(String dataUrl) {
        return Base64.decodeBase64(getMatcherForDataUrl(dataUrl).group(2));
    }

    @Named("dataUrlFromItemImage")
    String dataUrlFromItemImage(ItemImage image) {
        StringBuilder dataBuilder = new StringBuilder();
        dataBuilder.append("data:");
        dataBuilder.append(image.getMimeType());
        dataBuilder.append(";");
        dataBuilder.append("base64,");
        dataBuilder.append(Base64.encodeBase64String(image.getData()));
        return dataBuilder.toString();
    }

    @Mappings({
        @Mapping(target = "data", source = ".", qualifiedByName = "dataUrlFromItemImage"),
        @Mapping(target = "itemId", source = "item.id")
    })
    public abstract ItemImageDTO toDTO(ItemImage itemImage);

    @Mappings({
        @Mapping(target = "data", source = "dto.data", qualifiedByName = "dataUrlToData"),
        @Mapping(target = "mimeType", source = "dto.data", qualifiedByName = "dataUrlToMimeType"),
        @Mapping(target = "description", source = "dto.description")
    })
    public abstract ItemImage fromDTO(ItemImageDTO dto, Item item);
}
