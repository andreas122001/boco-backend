package edu.ntnu.idatt2106.bocobackend.dto.mapper;

import java.util.Collection;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import edu.ntnu.idatt2106.bocobackend.dto.ItemDTO;
import edu.ntnu.idatt2106.bocobackend.model.Item;
import edu.ntnu.idatt2106.bocobackend.model.ItemImage;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.service.UserService;

/**
 * Class to map between a ItemDTO and Item.
 * See ItemMapperImpl.java for implemented methods.
 */
@Mapper(
    componentModel = "spring",
    uses = {
        UserMapper.class,
        AddressMapper.class
    }
)
public abstract class ItemMapper {
    @Autowired
    UserService userService;

    @Named("itemsToItemIds")
    List<Long> itemsToItemIds(List<ItemImage> images) {
        return images.stream()
            .map(x -> x.getId())
            .toList();
    }

    @Mappings({
        @Mapping(target = "userEmail", source = "user.email"),
        @Mapping(target = "imageIds", source = "itemImages", qualifiedByName = "itemsToItemIds")
    })
    public abstract ItemDTO toDTO(Item item);
    public abstract List<ItemDTO> toDTOs(Collection<Item> items);

    @Mappings(
        @Mapping(target = "user", source = "userEmail", ignore = true)
    )
    public abstract void updateFromDTO(ItemDTO dto, @MappingTarget Item item);

    @Mappings({
        @Mapping(target = "address", source = "dto.address"),
        @Mapping(target = "user", source = "itemOwner")
    })
    public abstract Item fromDTO(ItemDTO dto, User itemOwner);

}
