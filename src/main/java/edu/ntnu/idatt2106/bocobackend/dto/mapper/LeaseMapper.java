package edu.ntnu.idatt2106.bocobackend.dto.mapper;

import java.util.List;
import java.util.UUID;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

import edu.ntnu.idatt2106.bocobackend.dto.LeaseDTO;
import edu.ntnu.idatt2106.bocobackend.dto.LeasePeriodDTO;
import edu.ntnu.idatt2106.bocobackend.model.Lease;

/**
 * Class to map between a LeaseDTO and Lease.
 * See LeaseMapperImpl.java for implemented methods.
 */
@Mapper(componentModel = "spring")
public abstract class LeaseMapper {
    @Named("dtoIDToModelID")
    String dtoIDToModelID(UUID id) {
        return id.toString();
    }

    @Mappings({
        @Mapping(target = "id", source = "id", qualifiedByName = "dtoIDToModelID"),
        @Mapping(target = "itemId", source = "item.id"),
        @Mapping(target = "lesseeEmail", source = "lessee.email")
    })
    public abstract LeaseDTO toDTO(Lease lease);
    public abstract List<LeaseDTO> toDTOs(List<Lease> lease);

    @Mappings(
        @Mapping(target = "id", source = "id", ignore = true)
    )
    public abstract Lease fromDTO(LeaseDTO dto);

    @Mappings({
        @Mapping(target = "startTime",source = "lease.startTime"),
        @Mapping(target = "endTime",source = "lease.endTime")
    })
    public abstract LeasePeriodDTO toLeasePeriodDTO(Lease lease);
    public abstract List<LeasePeriodDTO> toLeasePeriodDTOs(List<Lease> lease);
}
