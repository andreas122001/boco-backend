package edu.ntnu.idatt2106.bocobackend.dto.mapper;

import java.util.Base64;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import edu.ntnu.idatt2106.bocobackend.dto.SessionDTO;
import edu.ntnu.idatt2106.bocobackend.model.Session;

/**
 * Class to map between a SessionDTO and Session.
 * See SessionMapperImpl.java for implemented methods.
 */
@Mapper(componentModel = "spring")
public abstract class SessionMapper {
    
    @Named("encodeToken")
    String encodeToken(byte[] token) {
        return Base64.getUrlEncoder().encodeToString(token);
    }

    @Named("decodeToken")
    public byte[] decodeToken(String dtoToken) {
        return Base64.getUrlDecoder().decode(dtoToken);
    }

    @Mapping(target = "token", source = "token", qualifiedByName = "encodeToken")
    public abstract SessionDTO toDTO(Session session);

    public abstract Session fromDTO(SessionDTO dto);
}
