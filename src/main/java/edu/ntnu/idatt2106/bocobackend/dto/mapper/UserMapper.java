package edu.ntnu.idatt2106.bocobackend.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import edu.ntnu.idatt2106.bocobackend.dto.UserDTO;
import edu.ntnu.idatt2106.bocobackend.model.User;

/**
 * Class to map between a UserDTOs and User.
 * See UserMapperImpl.java for implemented methods.
 */
@Mapper(
    componentModel = "spring",
    uses = AddressMapper.class
)
public abstract class UserMapper {
    @Autowired
    PasswordEncoder passwordEncoder;

    @Named("passwordToPasswordHash")
    String passwordToPasswordHash(String password) {
        return passwordEncoder.encode(password);
    }

    public abstract UserDTO toDTO(User user);
    
    @Mappings({
        @Mapping(target = "passwordHash", ignore = true),
    })
    public abstract void updateFromDTO(UserDTO dto, @MappingTarget User user);

    @Mappings({
        @Mapping(source = "password", target = "passwordHash", qualifiedByName = "passwordToPasswordHash"),
    })
    public abstract User fromDTO(UserDTO dto);
}
