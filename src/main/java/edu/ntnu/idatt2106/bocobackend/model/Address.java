package edu.ntnu.idatt2106.bocobackend.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import static edu.ntnu.idatt2106.bocobackend.util.StringUtil.trimAndThrowIfNullOrBlank;

import java.util.regex.Pattern;

@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String street;

    @Column(nullable = false)
    private String postalCode;

    Address() {}

    /**
     * Creates a new address.
     * @param street The street of the address.
     * @param postalCode The postal code of the address.
     */
    public Address(String street, String postalCode) {
        if (!Pattern.matches("^(\\d{4})$", postalCode)) {
            throw new IllegalArgumentException("postal code must consist of exactly 4 digits");
        }

        this.street = trimAndThrowIfNullOrBlank(street, "street");
        this.postalCode = postalCode;
    }

    @Override
    public String toString() {
        return postalCode + "," + street;
    }

    /**
     * Gets the street of the address. It consists of name and number.
     * @return
     */
    public String getStreet() {
        return this.street;
    }

    /**
     * Sets the street of the address.
     * @param street Street of the address. Cannot be null.
     */
    public void setStreet(String street) {
        this.street = trimAndThrowIfNullOrBlank(street, "street");
    }

    /**
     * Gets the postal code of the address.
     * @return
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Sets the postal code for the address.
     * @param postalCode
     */
    public void setPostalCode(String postalCode) {
        if (!Pattern.matches("^(\\d{4})$", postalCode)) {
            throw new IllegalArgumentException("postal code must consist of exactly 4 digits");
        }

        this.postalCode = postalCode;
    }
}
