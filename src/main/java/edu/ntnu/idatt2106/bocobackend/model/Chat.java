package edu.ntnu.idatt2106.bocobackend.model;

import java.util.Objects;
import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;

@Entity
@Table(name = "`chat`", indexes = @Index(columnList = "uuid"))
public class Chat {
    
    @EmbeddedId
    private ChatPrimaryKey primaryKey;

    @Column(length = 16)
    private UUID uuid;

    Chat() {}

    /**
     * 
     * @param primaryKey The compisite id of the chat (see ChatPrimaryKey.java)
     */
    public Chat(ChatPrimaryKey primaryKey) {
        this.primaryKey = Objects.requireNonNull(primaryKey, "id cannot be null");
        this.uuid = UUID.randomUUID();
    }

    public ChatPrimaryKey getPrimaryKey() {
        return this.primaryKey;
    }

    public void setPrimaryKey(ChatPrimaryKey primaryKey) {
        this.primaryKey = Objects.requireNonNull(primaryKey, "primaryKey cannot be null");
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = Objects.requireNonNull(uuid, "uuid cannot be null");
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((primaryKey == null) ? 0 : primaryKey.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Chat other = (Chat) obj;
        if (primaryKey == null) {
            if (other.primaryKey != null)
                return false;
        } else if (!primaryKey.equals(other.primaryKey))
            return false;
        return true;
    }

}