package edu.ntnu.idatt2106.bocobackend.model;

import java.time.Instant;
import java.util.Objects;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import edu.ntnu.idatt2106.bocobackend.util.StringUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinColumns;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "`chat_message`", indexes = {
    @Index(columnList = "chat_item_id"),
    @Index(columnList = "chat_initiator_id")
})
public class ChatMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(nullable = false)
    private Instant timestamp;

    @Column(nullable = false, length = 1024)
    private String content;

    @ManyToOne
    @JoinColumns({
        @JoinColumn(nullable = false),
        @JoinColumn(nullable = false)
    })
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Chat chat;

    @ManyToOne
    @JoinColumn(nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User sender;

    ChatMessage() {}

    /**
     * 
     * @param timestamp the time the message is created, as an Instant
     * @param content the text of the message 
     * @param chat the chat the message belongs to
     * @param sender the user sending the message
     */
    public ChatMessage(Instant timestamp, String content, Chat chat, User sender) {
        this.timestamp = Objects.requireNonNull(timestamp, "timestamp cannot be null");
        this.content = StringUtil.trimAndThrowIfNullOrBlank(content);
        this.chat = Objects.requireNonNull(chat, "chat cannot be null");
        this.sender = Objects.requireNonNull(sender, "sender cannot be null");
    }

    public long getId() {
        return this.id;
    }

    public Instant getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        Objects.requireNonNull(timestamp, "timestamp cannot be null");
        
        this.timestamp = timestamp;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        StringUtil.trimAndThrowIfNullOrBlank(content);

        this.content = content;
    }


    public Chat getChat() {
        return this.chat;
    }

    public User getSender() {
        return this.sender;
    }
    
}
