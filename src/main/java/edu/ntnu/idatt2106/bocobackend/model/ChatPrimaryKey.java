package edu.ntnu.idatt2106.bocobackend.model;

import java.io.Serializable;
import java.util.Objects;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Embeddable
public class ChatPrimaryKey implements Serializable {

    @ManyToOne
    @JoinColumn(nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Item item;
    
    @ManyToOne
    @JoinColumn(nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User initiator;

    /**
     * 
     * @param item the item the chat is initialized from
     * @param initiator the user sending a message to the lessor of the item
     */
    public ChatPrimaryKey(Item item, User initiator) {
        this.item = Objects.requireNonNull(item);
        this.initiator = Objects.requireNonNull(initiator);
    }

    /**
     * Default constructor.
     * Package-private as it is - and should only be - used by JPA. 
     */
    ChatPrimaryKey() {}

    public Item getItem() {
        return this.item;
    }

    public void setItem(Item item) {
        this.item = Objects.requireNonNull(item, "item cannot be null");
    }
    
    public User getInitiator() {
        return this.initiator;
    }

    public void setInitiator(User initiator) {
        this.initiator = Objects.requireNonNull(initiator, "initiator cannot be null");
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(item, initiator);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ChatPrimaryKey other = (ChatPrimaryKey) obj;
        if (item == null) {
            if (other.item != null)
                return false;
        } else if (!item.equals(other.item))
            return false;
        if (initiator == null) {
            if (other.initiator != null)
                return false;
        } else if (!initiator.equals(other.initiator))
            return false;
        return true;
    }

}
