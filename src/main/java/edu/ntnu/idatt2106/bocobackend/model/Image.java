package edu.ntnu.idatt2106.bocobackend.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;

import static edu.ntnu.idatt2106.bocobackend.util.StringUtil.trimAndThrowIfNullOrBlank;
import static java.util.Objects.requireNonNull;

@Entity
public class Image {
    public static final int MAXIMUM_IMAGE_SIZE = 32 * 1024 * 1024;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private String mimeType;

    @Lob
    @Column(nullable = false)
    private byte[] data;

    Image() {}

    public Image(String description, String mimeType, byte[] data) {
        this.description = trimAndThrowIfNullOrBlank(description,"description");
        this.mimeType = trimAndThrowIfNullOrBlank(mimeType, "mimeType");
        this.data = requireNonNull(data,"data can not be null");
        if (data.length > MAXIMUM_IMAGE_SIZE) {
            throw new IllegalArgumentException("image file too big");
        }
    }

    public Long getId() {
        return this.id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = trimAndThrowIfNullOrBlank( description,"description");
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = trimAndThrowIfNullOrBlank( mimeType,"mimeType");
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = requireNonNull(data, "data cannot be null");
        if (data.length > MAXIMUM_IMAGE_SIZE) {
            throw new IllegalArgumentException("image file too big");
        }
    }
}
