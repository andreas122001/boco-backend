package edu.ntnu.idatt2106.bocobackend.model;

import java.time.Instant;
import java.util.Objects;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;

import edu.ntnu.idatt2106.bocobackend.util.InstantUtil;
import edu.ntnu.idatt2106.bocobackend.util.ItemCategory;
import edu.ntnu.idatt2106.bocobackend.util.PriceUnit;
import edu.ntnu.idatt2106.bocobackend.util.StringUtil;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "`item`")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private int price;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private PriceUnit priceUnit;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ItemCategory category;

    @Column(nullable = false)
    private Instant startTime;

    @Column(nullable = false)
    private Instant endTime;

    @Column(nullable = false)
    private String description;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    @JoinColumn(nullable = false)
    private User user;

    @OneToMany(mappedBy = "item", fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    private List<ItemImage> itemImages;

    @OneToMany(mappedBy = "item", cascade = CascadeType.ALL)
    private Set<Lease> leases;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(nullable = true)
    private Address address;

    Item() {
    }

    /**
     * 
     * @param title       of item, cannot be null.
     * @param price       of item ,cannot be null.
     * @param priceUnit   of item, cannot be null
     * @param category    of item, cannot be null
     * @param startime    of item, cannot be null
     * @param endTime     of item, cannot be null
     * @param description of item, can be null
     * @param user        the lessor of the item
     */
    public Item(
        String title,
        int price,
        PriceUnit priceUnit,
        ItemCategory category,
        Instant startTime,
        Instant endTime,
        String description,
        User user
    ) {
        this.title = StringUtil.trimAndThrowIfNullOrBlank(title, "title cannot be empty");
        this.price = Objects.requireNonNull(price, "price cannot be empty");
        if (price < 0) {
            throw new IllegalArgumentException("price cannot be negative");
        }
        this.priceUnit = priceUnit;
        this.category = category;
        this.startTime = InstantUtil.startTimeChronological(startTime, endTime);
        this.endTime = InstantUtil.endTimeChronological(startTime, endTime);
        this.description = Objects.requireNonNull(description, "description cannot be empty");
        this.user = Objects.requireNonNull(user, "user cannot be empty");
        this.itemImages = new ArrayList<>();

        this.leases = Set.of();
    }

    /**
     * Gets the id of the user
     * 
     * @return
     */

    public long getId() {
        return this.id;
    }

    /**
     * Gets the id of the user
     * 
     * @return
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Sets the title for the item
     * 
     * @param title
     */
    public void setTitle(String title) {
        this.title = StringUtil.trimAndThrowIfNullOrBlank(title, "title cannot be null");
    }

    /**
     * Gets the id of the user
     * 
     * @return
     */
    public int getPrice() {
        return this.price;
    }

    /**
     * Sets the price for the item
     * 
     * @param price
     */
    public void setPrice(int price) {
        if(price < 0){
            throw new IllegalArgumentException("price can not be negative");
        }
        this.price = price;
    }

    /**
     * Gets the priceunit of the item
     * 
     * @return
     */
    public PriceUnit getPriceUnit() {
        return this.priceUnit;
    }

    /**
     * Sets the priceunit for the item
     * 
     * @param priceUnit
     */
    public void setPriceUnit(PriceUnit priceUnit) {
        this.priceUnit = Objects.requireNonNull(priceUnit, "Price unit cannot be null");
    }

    /**
     * Gets the category of the item
     *
     * @return Category of item.
     */
    public ItemCategory getCategory() {
        return this.category;
    }

    /**
     * Sets the category for the item
     *
     * @param category to set.
     */
    public void setCategory(ItemCategory category) {
        this.category = Objects.requireNonNull(category, "Category cannot be null");
    }

    /**
     * Gets the startime of the item
     * 
     * @return
     */
    public Instant getStartTime() {
        return this.startTime;
    }

    /**
     * Sets the start time for the item
     * 
     * @param startTime
     */
    public void setStartTime(Instant startTime) {
         
        this.startTime = InstantUtil.startTimeChronological(startTime, getEndTime());
    }

    /**
     * Gets the end time for the item
     * 
     * @return
     */
    public Instant getEndTime() {
        return this.endTime;
    }

    /**
     * Sets the endtime for the item
     * 
     * @param endtime
     */
    public void setEndTime(Instant endTime) {
        this.endTime = InstantUtil.endTimeChronological(startTime, endTime);
    }

    /**
     * Gets the description of the item
     * 
     * @return
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets the description for the item
     * 
     * @param description
     */
    public void setDescription(String description) {
        this.description = Objects.requireNonNull(description, "description cannot be null");
    }

    /**
     * Gets the user of the item
     * 
     * @return
     */
    public User getUser() {
        return this.user;
    }

    /**
     * Sets the user for the item
     * 
     * @param user
     */
    public void setUser(User user) {
        this.user = Objects.requireNonNull(user, "user cannot be null");
    }

       /**
    * gets item image list
    * @return
    */
    public List<ItemImage> getItemImages() {
        List<ItemImage> copy = new ArrayList<>(itemImages);
        return copy;
    }

    /**
     * set item image list
     * @param itemImageList
     */
    public void setItemImages(List<ItemImage>itemImageList) {
        Objects.requireNonNull(itemImageList,"image can not be null");
        List<ItemImage> copy = new ArrayList<>(itemImageList);
        this.itemImages = copy;
    }

    public Address getAddress() {
        if (address == null) {
            return user.getAddress();
        }
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Item other = (Item) obj;
        if (id != other.id)
            return false;
        return true;
    }

}
