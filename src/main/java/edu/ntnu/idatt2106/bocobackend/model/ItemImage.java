package edu.ntnu.idatt2106.bocobackend.model;

import java.util.Objects;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;

/**
 * Entity iteamImage.
 * N-1 releationship with Item
 */

@Entity
public class ItemImage {
    public static final int MAXIMUM_IMAGE_SIZE = 32 * 1024 * 1024;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JoinColumn(nullable = false)
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Image image;

    @JoinColumn
    @ManyToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Item item;

    ItemImage() {}

    public ItemImage(String description, byte[] data, String mimeType, Item item)  {
        this.image = new Image(description, mimeType, data);
        this.item = Objects.requireNonNull(item,"item can not be null");
    }

    /**
     * gets id
     * @return
     */
    public long getId() {
        return id;
    }

    /**
     * gets the image description
     * @return
     */
    public String getDescription() {
        return image.getDescription();
    }

    /**
     * sets the image description
     * @param description
     */
    public void setDescription(String description) {
        image.setDescription(description);
    }

    /**
     * Gets image data MIME type
     * @return
     */
    public String getMimeType() {
        return image.getMimeType();
    }

    /**
     * Sets the image data MIME type
     * @param data
     */
    public void setMimeType(String mimeType) {
        image.setMimeType(mimeType);
    }

    /**
     * gets image data 
     * @return
     */
    public byte[] getData() {
        return image.getData();
    }

    /**
     * sets the image data
     * @param data
     */
    public void setData(byte[] data) {
        image.setData(data);
    }

    /**
     * gets the item for image
     * @return
     */
    public Item getItem() {
        return this.item;
    }

    /**
     * sets the image to a item
     * @param item
     */
    public void setItem(Item item) {
        this.item = Objects.requireNonNull(item);
    }
}
