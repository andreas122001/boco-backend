package edu.ntnu.idatt2106.bocobackend.model;

import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class Lease {
    @Id
    @Column(length = 16)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @ManyToOne
    @JoinColumn(nullable = true)
    private User lessee;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Item item;

    @Column(nullable = false)
    private Instant startTime;

    @Column(nullable = false)
    private Instant endTime;

    Lease() {
    }

    /**
     * 
     * @param lessee The user that is leasing the item.
     * @param item The item that is being leased.
     * @param startTime The date at which the lease starts.
     * @param endTime The date at which the lease ends.
     */
    public Lease(User lessee, Item item, Instant startTime, Instant endTime) {
        if (startTime.isAfter(endTime)) {
            throw new IllegalArgumentException("startTime cannot be after endTime");
        }

        this.lessee = Objects.requireNonNull(lessee, "lessee cannot be null");
        this.item = Objects.requireNonNull(item, "item cannot be null");
        this.startTime = Objects.requireNonNull(startTime, "startTime cannot be null");
        this.endTime = Objects.requireNonNull(endTime, "endTime cannot be null");
    }

    /**
     * Gets the ID of the lease.
     * 
     * @return Lease's ID.
     */
    public UUID getId() {
        return this.id;
    }

    /**
     * Gets the User that is leasing the item.
     * 
     * @return Lease's lessee.
     */
    public User getLessee() {
        return this.lessee;
    }

    /**
     * Sets `lessee` attribute to null.
     * Required to set null on user deletion.
     */
    public void detachLessee() {
        this.lessee = null;
    }

    /**
     * Gets the item that is being leased.
     * 
     * @return Lease's item.
     */
    public Item getItem() {
        return this.item;
    }

    /**
     * Gets the date at which the lease starts.
     * 
     * @return Lease's start time.
     */
    public Instant getStartTime() {
        return this.startTime;
    }

    /**
     * Gets the date at which the lease ends.
     * 
     * @return Lease's end time.
     */
    public Instant getEndTime() {
        return this.endTime;
    }
}
