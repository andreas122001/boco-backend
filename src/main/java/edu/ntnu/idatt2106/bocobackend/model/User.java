package edu.ntnu.idatt2106.bocobackend.model;

import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PreRemove;
import jakarta.persistence.Table;

import static edu.ntnu.idatt2106.bocobackend.util.StringUtil.trimAndThrowIfNullOrBlank;

@Entity
@Table(name = "`user`")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false, unique = true)
    private String email;

    @JsonIgnore
    @Column(nullable = false)
    private String passwordHash;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private UserRole role;

    @JoinColumn(nullable = false)
    @OneToOne(cascade = CascadeType.ALL)
    private Address address;

    @OneToMany(mappedBy = "lessee")
    private Set<Lease> leases;

    User() {
    }

    @Override
    public String toString() {
        return email;
    }

    /**
     * Creates a new user.
     * 
     * @param firstName    First name of the user. Cannot be null.
     * @param lastName     Last name of the user. Cannot be null.
     * @param email        Email of the user. Cannot be null.
     * @param passwordHash Password hash of the user. Cannot be null.
     * @param address      Address of the user. Cannot be null.
     */
    public User(String firstName, String lastName, String email, String passwordHash, Address address) {
        this.firstName = trimAndThrowIfNullOrBlank(firstName, "first name");
        this.lastName = trimAndThrowIfNullOrBlank(lastName, "last name");
        this.email = trimAndThrowIfNullOrBlank(email, "email");
        this.passwordHash = trimAndThrowIfNullOrBlank(passwordHash, "password hash");
        this.address = Objects.requireNonNull(address, "address cannot be null");
        this.leases = Set.of();
        this.role = UserRole.USER;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    /**
     * Gets the id of the user.
     * 
     * @return
     */
    public long getId() {
        return id;
    }

    /**
     * Gets the first name of the user.
     * 
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first name of the user.
     * 
     * @param firstName The first name of the user. It cannot be null.
     */
    public void setFirstName(String firstName) {
        this.firstName = trimAndThrowIfNullOrBlank(firstName, "first name");
    }

    /**
     * Gets the last name of the user.
     * 
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name of the user.
     * 
     * @param lastName The last name of the user. It cannot be null.
     */
    public void setLastName(String lastName) {
        this.lastName = trimAndThrowIfNullOrBlank(lastName, "last name");
    }

    /**
     * Gets the email of the user.
     * 
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email of the user.
     * 
     * @param email The email of the user. It cannot be null.
     */
    public void setEmail(String email) {
        this.email = trimAndThrowIfNullOrBlank(email, "email");
    }

    /**
     * Gets the password hash of the user.
     * 
     * @return
     */
    public String getPasswordHash() {
        return passwordHash;
    }

    /**
     * Sets the password hash of the user.
     * 
     * @param passwordHash Pass        .springframework.dao.DataIntegrityViolationException: could not execute statement; SQL [n/a]; constraint ["FKAO8V2XJLATGR7QB5P4BWNCXL7: PUBLIC.LEASE FOREIGN KEY(lessee_id) REFERENCES PUBLIC.""user""(ID) (CAST(3 AS BIGINT))"; SQL statement:word hash of the user. It cannot be null.
     */
    public void setPasswordHash(String passwordHash) {
        this.passwordHash = trimAndThrowIfNullOrBlank(passwordHash, "password hash");
    }

    /**
     * Gets the address of the user.
     * 
     * @return
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the address of the user.
     * 
     * @param address Address of the user. It cannot be null.
     */
    public void setAddress(Address address) {
        this.address = Objects.requireNonNull(address, "address cannot be null");
    }

    /**
     * JPA pre-remove method.
     * 
     * Sets all leases' lessee attribute associated with this user to null
     */
    @PreRemove
    private void preRemove() {
        this.leases.forEach(Lease::detachLessee);
    }
    /**
     * Gets the role of the user.
     * @return
     */
    public UserRole getRole() {
        return this.role;
    }

    /**
     * Sets the role of the user.
     * @param role New user role.
     */
    public void setRole(UserRole role) {
        this.role = Objects.requireNonNull(role, "role");
    }
}
