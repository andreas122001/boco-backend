package edu.ntnu.idatt2106.bocobackend.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import static java.util.Objects.requireNonNull;

/**
 * JSON model used to deserialize user credentials.
 */
public class UserCredentials {
    private final String email;
    private final String password;

    /**
     * Constructor and JSONCreator.
     * @param email User's email.
     * @param password User's password.
     */
    @JsonCreator
    public UserCredentials(
        @JsonProperty("email") String email, 
        @JsonProperty("password") String password
    ) {
        this.email = requireNonNull(email, "email cannot be null");
        this.password = requireNonNull(password, "password cannot be null");
    }

    @JsonProperty("email")
    public String getEmail() {
        return this.email;
    }

    @JsonProperty("password")
    public String getPassword() {
        return this.password;
    }
}
