package edu.ntnu.idatt2106.bocobackend.model;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

/**
 * Roles that a user can have. Each role contains privileges for that role.
 * Roles are ordered and inherit the privileges of roles below them.
 */
public enum UserRole implements GrantedAuthority {
    ADMIN("ADMIN"),
    USER("USER");

    private final String name;

    private int index;

    UserRole(String name) {
        this.name = name;
    }

    static {
        UserRole[] roles = UserRole.values();
        for (int i = 0; i < roles.length; i++) {
            roles[i].index = i;
        }
    }

    public String getName() {
        return name;
    }

    /**
     * Gets all authorities for the role. 
     * This includes the current authority, as well as authorities defined below the
     * current authority.
     * @return
     */
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays.stream(UserRole.values())
            .skip(index)
            .toList();
    }

    /**
     * Gets the authority for the role.
     * @return
     */
    @Override
    public String getAuthority() {
        return "ROLE_" + name;
    }
}
