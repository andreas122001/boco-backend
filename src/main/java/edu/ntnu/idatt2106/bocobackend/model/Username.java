package edu.ntnu.idatt2106.bocobackend.model;

import static java.util.Objects.requireNonNull;

public class Username {
    public static final String SELF = "self";

    private final String username;

    public Username(String username) {
        this.username = requireNonNull(username);
    }

    public boolean isSelf() {
        return SELF.equals(username);
    }

    public String getUsername() {
        return username;
    }
}
