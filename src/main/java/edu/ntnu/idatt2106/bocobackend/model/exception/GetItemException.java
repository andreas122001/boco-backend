package edu.ntnu.idatt2106.bocobackend.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class GetItemException extends RuntimeException {
    
}
