package edu.ntnu.idatt2106.bocobackend.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = 
"Deleting the last image of an item is not permitted ")
public class ImageDeletionException extends RuntimeException {
}