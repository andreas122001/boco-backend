package edu.ntnu.idatt2106.bocobackend.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Lease period unvalid")
public class ItemLeasedInTimePeriodException extends RuntimeException {
}
