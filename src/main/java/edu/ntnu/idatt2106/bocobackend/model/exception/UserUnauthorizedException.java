package edu.ntnu.idatt2106.bocobackend.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED, reason = "user not authorized")
public class UserUnauthorizedException extends RuntimeException{
}
