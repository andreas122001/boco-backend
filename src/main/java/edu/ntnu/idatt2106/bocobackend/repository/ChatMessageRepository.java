package edu.ntnu.idatt2106.bocobackend.repository;

import org.springframework.stereotype.Repository;

import edu.ntnu.idatt2106.bocobackend.model.Chat;
import edu.ntnu.idatt2106.bocobackend.model.ChatMessage;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface ChatMessageRepository extends JpaRepository<ChatMessage, Long> {
    
    /**
     * Gets all messages in a chat
     * @param chat the chat to retrieve messages from
     * @return all messages in the chat as a list
     */
    @Query("SELECT cm FROM ChatMessage cm WHERE cm.chat=?1")
    List<ChatMessage> getAllMessagesByChat(Chat chat);
}