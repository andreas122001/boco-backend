package edu.ntnu.idatt2106.bocobackend.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import edu.ntnu.idatt2106.bocobackend.model.Chat;
import edu.ntnu.idatt2106.bocobackend.model.ChatPrimaryKey;
import edu.ntnu.idatt2106.bocobackend.model.User;

@Repository
public interface ChatRepository extends JpaRepository<Chat, ChatPrimaryKey> {

    /**
     * Finds a chat by it's uuid
     * @param uuid the uuid of the chat
     * @return the given chat or null
     */
    Optional<Chat> findByUuid(UUID uuid);

    /**
     * Finds all chats where the user passed as param is a participant 
     * @param user the user to find chats by
     * @return a list of all the chats where the user is a participant
     */
    @Query("SELECT c FROM Chat c WHERE c.primaryKey.initiator=?1 OR c.primaryKey.item.user=?1")
    List<Chat> findByUser(User user);
}
