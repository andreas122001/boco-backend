package edu.ntnu.idatt2106.bocobackend.repository;

import edu.ntnu.idatt2106.bocobackend.model.Item;
import edu.ntnu.idatt2106.bocobackend.model.ItemImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemImageRepository extends JpaRepository<ItemImage, Long> {

    /**
     * Finds all images belonging to an item
     * @param item the item to fetch images by
     * @return all images belonging to an item as a list
     */
    List<ItemImage> findByItem(Item item);

}
