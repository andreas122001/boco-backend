package edu.ntnu.idatt2106.bocobackend.repository;


import org.springframework.data.domain.Pageable;

import edu.ntnu.idatt2106.bocobackend.util.ItemCategory;

public class ItemQueryParams {
    private String email;
    private String search;
    private ItemCategory category;
    private Pageable pageable;

    public ItemQueryParams() {
        this.email = null;
        this.search = null;
        this.category = null;
        this.pageable = Pageable.unpaged();
    }

    public String getEmail() {
        return email;
    }

    public ItemQueryParams email(String email) {
        this.email = email;
        return this;
    }

    public String getSearch() {
        return search;
    }

    public ItemQueryParams search(String search) {
        this.search = search;
        return this;
    }

    public ItemCategory getCategory() {
        return category;
    }

    public ItemQueryParams category(ItemCategory category) {
        this.category = category;
        return this;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public ItemQueryParams pageable(Pageable pageable) {
        this.pageable = pageable;
        return this;
    }
}
