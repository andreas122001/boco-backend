package edu.ntnu.idatt2106.bocobackend.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.ntnu.idatt2106.bocobackend.model.Item;
import edu.ntnu.idatt2106.bocobackend.util.ItemCategory;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
    /**
     * Finds all items filtered by the provided parameters.
     * 
     * @param email the users email
     * @param search the string to search items by
     * @param category the category to filter items by
     * @param pageable paging settings
     * 
     * @return All items matching the provided parameters.
     */
    @Query(
        "SELECT i FROM Item i WHERE " +
        "(?#{#email == null} = TRUE OR i.user.email=:email) AND " +
        "(?#{#search == null} = TRUE OR (LOWER(i.title) LIKE %:search% OR LOWER(i.description) LIKE %:search%)) AND " +
        "(?#{#category == null} = TRUE OR CAST(i.category AS string)=?#{#category != null ? #category.name : null})"
    )
    Page<Item> findItems(
        @Param("email") String email,
        @Param("search") String search,
        @Param("category") ItemCategory category,
        Pageable pageable
    );

    /**
     * Finds all items filtered by the provided parameters.
     * 
     * @param params Query parameters to use for filtering.
     * 
     * @return All items matching the provided parameters.
     */
    default Page<Item> findItems(ItemQueryParams params) {
        return findItems(
            params.getEmail(),
            params.getSearch(), 
            params.getCategory(), 
            params.getPageable()
        );
    }
}
