package edu.ntnu.idatt2106.bocobackend.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.ntnu.idatt2106.bocobackend.model.Item;
import edu.ntnu.idatt2106.bocobackend.model.Lease;
import edu.ntnu.idatt2106.bocobackend.model.User;

public interface LeaseRepository extends JpaRepository<Lease, UUID> {
    
    /**
     * Gets all leases a user has leased
     * @param user the user to find leases by
     * @return all leases a user has leased, as a list
     */
    @Query("SELECT l FROM Lease l WHERE l.lessee=:user")
    public List<Lease> getLeasesByUser(@Param("user") User user);

    /**
     * Gets all leases on an item
     * @param item the item to find leases by
     * @return all leases on an item, as a list
     */
    @Query("SELECT l FROM Lease l WHERE l.item=:item")
    List<Lease> getLeasesByItem(@Param("item")Item item);
   
}