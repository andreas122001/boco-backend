package edu.ntnu.idatt2106.bocobackend.repository;

import edu.ntnu.idatt2106.bocobackend.model.Session;
import jakarta.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface SessionRepository extends JpaRepository<Session, byte[]> {
    
    /**
     * Deletes all expired sessions
     */
    @Modifying
    @Transactional
    @Query("DELETE FROM Session WHERE CURRENT_TIMESTAMP >= absoluteExpiry OR CURRENT_TIMESTAMP >= idleExpiry")
    void deleteAllExpired();
}
