package edu.ntnu.idatt2106.bocobackend.security;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Handles exception that occurr while authenticating the user.
 */
@Component
public class AuthenticationExceptionEntryPoint implements AuthenticationEntryPoint {
    Logger logger = LoggerFactory.getLogger(AuthenticationExceptionEntryPoint.class);

    @Override
    public void commence(
        HttpServletRequest request, 
        HttpServletResponse response,
        AuthenticationException authException
    ) throws IOException, ServletException {
        logger.debug("rejecting unauthorized request");
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
    }
}
