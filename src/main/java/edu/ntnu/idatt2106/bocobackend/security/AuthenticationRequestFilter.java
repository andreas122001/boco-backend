package edu.ntnu.idatt2106.bocobackend.security;

import java.io.IOException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import edu.ntnu.idatt2106.bocobackend.model.Session;
import edu.ntnu.idatt2106.bocobackend.service.SessionService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class AuthenticationRequestFilter extends OncePerRequestFilter {
    private static final Logger logger = LoggerFactory.getLogger(AuthenticationRequestFilter.class);

    private final SessionService sessionService;

    public AuthenticationRequestFilter(@Autowired SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Override
    protected void doFilterInternal(
        HttpServletRequest request,
        HttpServletResponse response,
        FilterChain filterChain
    ) throws ServletException, IOException {
        Session session = getSessionFromRequest(request).orElseGet(() -> {
            logger.trace("No valid user session found.");
            return null;
        });

        if (session != null) {
            SessionAuthenticationToken token = new SessionAuthenticationToken(session);
            token.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(token);
            logger.trace("User authed successfully!");
        }

        filterChain.doFilter(request, response);
    }
    
    private Optional<Session> getSessionFromRequest(HttpServletRequest request) {
        final String authPrefix = "Bearer ";

        String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (authorizationHeader == null || !authorizationHeader.startsWith(authPrefix)) {
            logger.trace("No bearer session token provided.");
            return Optional.empty();
        }
        String dtoToken = authorizationHeader.substring(authPrefix.length());
        return sessionService.findAndRefresh(dtoToken).filter(x -> !x.isExpired());
    }
}
