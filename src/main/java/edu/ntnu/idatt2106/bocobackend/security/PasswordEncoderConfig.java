package edu.ntnu.idatt2106.bocobackend.security;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

@Configuration
public class PasswordEncoderConfig {
    private static final int SALT_LENGTH = 16;
    private static final int ITERS = 200_000;
    private static final int HASH_WIDTH = 512;

    @Bean
    PasswordEncoder getPasswordEncoder(@Value("${password.secret}") String passwordSecret) {
        return new Pbkdf2PasswordEncoder(passwordSecret, SALT_LENGTH, ITERS, HASH_WIDTH);
    }
}
