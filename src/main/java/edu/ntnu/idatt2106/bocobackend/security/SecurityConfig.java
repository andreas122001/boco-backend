package edu.ntnu.idatt2106.bocobackend.security;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.header.writers.StaticHeadersWriter;
import org.springframework.web.cors.CorsConfiguration;

import jakarta.servlet.http.HttpServletRequest;

/**
 * Configuration for sestting CORS and token-based session authentication.
 */
@SuppressWarnings("deprecation")
@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)  
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final AuthenticationExceptionEntryPoint authenticationExceptionEntryPoint;
    private final AuthenticationRequestFilter authenticationRequestFilter;
    private final String allowedOrigin;

    public SecurityConfig(
        @Value("${cors.allowed-origin}") String allowedOrigins,
        @Autowired AuthenticationExceptionEntryPoint authenticationExceptionEntryPoint,
        @Autowired AuthenticationRequestFilter authenticationRequestFilter
    ) {
        this.allowedOrigin = allowedOrigins;
        this.authenticationExceptionEntryPoint = authenticationExceptionEntryPoint;
        this.authenticationRequestFilter = authenticationRequestFilter;
    }

    CorsConfiguration corsConfiguration(HttpServletRequest request) {
        CorsConfiguration cors = new CorsConfiguration();
        cors.setAllowedOriginPatterns(List.of(allowedOrigin));
        cors.setAllowedMethods(
            Arrays.stream(HttpMethod.values()).map(x -> x.name()).toList()
        );
        cors.setAllowedHeaders(List.of("*"));
        return cors;
	}

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Set CORS and disable CSRF
        http.cors().configurationSource(this::corsConfiguration).and()
            .csrf().disable()
        // Allow all requests to /session
            .authorizeRequests()
                .antMatchers("/sessions/**").permitAll()
                .antMatchers(HttpMethod.GET, "/items/**").permitAll()
                .antMatchers("/ws/**").permitAll()

                .mvcMatchers(HttpMethod.POST, "/users").permitAll()
                .mvcMatchers(HttpMethod.GET, "/users/{username}").permitAll()
                .antMatchers("/swagger-ui/**", "/v3/api-docs/**").permitAll()
        // Require auth for all other urls.
            .anyRequest().authenticated().and()
            .exceptionHandling().authenticationEntryPoint(authenticationExceptionEntryPoint).and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .headers()
            .addHeaderWriter(new StaticHeadersWriter("Access-Control-Allow-Credentials", "true"));

        http.addFilterBefore(authenticationRequestFilter, UsernamePasswordAuthenticationFilter.class);
   }

}
