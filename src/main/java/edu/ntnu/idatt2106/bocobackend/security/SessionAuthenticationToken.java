package edu.ntnu.idatt2106.bocobackend.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;

import edu.ntnu.idatt2106.bocobackend.model.Session;
import edu.ntnu.idatt2106.bocobackend.model.User;

public class SessionAuthenticationToken extends AbstractAuthenticationToken {
    private final Session session;

    public SessionAuthenticationToken(Session session) {
        super(session.getUser().getRole().getAuthorities());
        this.session = session;
        setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return session.getToken();
    }

    @Override
    public User getPrincipal() {
        return session.getUser();
    }

    public Session getSession() {
        return session;
    }
}
