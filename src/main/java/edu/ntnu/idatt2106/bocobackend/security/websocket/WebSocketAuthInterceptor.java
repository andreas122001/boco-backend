package edu.ntnu.idatt2106.bocobackend.security.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.stereotype.Component;

import edu.ntnu.idatt2106.bocobackend.security.SessionAuthenticationToken;
import edu.ntnu.idatt2106.bocobackend.service.SessionService;

@Component
public class WebSocketAuthInterceptor implements ChannelInterceptor {
    
    private final static String SESSION_TOKEN_HEADER = "Session-Token";

    private final SessionService sessionService;

    public WebSocketAuthInterceptor(@Autowired SessionService sessionService) {
        this.sessionService = sessionService;
    } 

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel messageChannel) {
        StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);

        if (accessor.getCommand() == StompCommand.CONNECT) {
            String sessionToken = accessor.getFirstNativeHeader(SESSION_TOKEN_HEADER);

            this.sessionService
                .findAndRefresh(sessionToken)
                .filter(session -> !session.isExpired())
                .ifPresent(session -> {
                    accessor.setUser(new SessionAuthenticationToken(session));
                });
        }

        return message;
    }
}
