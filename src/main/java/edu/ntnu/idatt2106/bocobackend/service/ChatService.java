package edu.ntnu.idatt2106.bocobackend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ntnu.idatt2106.bocobackend.dto.ChatDTO;
import edu.ntnu.idatt2106.bocobackend.dto.ChatMessageDTO;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.ChatMapper;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.ChatMessageMapper;
import edu.ntnu.idatt2106.bocobackend.model.Chat;
import edu.ntnu.idatt2106.bocobackend.model.ChatMessage;
import edu.ntnu.idatt2106.bocobackend.model.ChatPrimaryKey;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.model.Item;
import edu.ntnu.idatt2106.bocobackend.model.exception.ChatAlreadyExsistsException;
import edu.ntnu.idatt2106.bocobackend.model.exception.ForbiddenException;
import edu.ntnu.idatt2106.bocobackend.repository.ChatMessageRepository;
import edu.ntnu.idatt2106.bocobackend.repository.ChatRepository;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class ChatService {
    private static final Logger logger = LoggerFactory.getLogger(ChatService.class);

    private final ChatRepository chatRepository;
    private final ChatMessageRepository chatMessageRepository;
    private final ChatMapper chatMapper;
    private final ChatMessageMapper chatMessageMapper;
    private final ItemService itemService;

    public ChatService(
        @Autowired ChatRepository chatRepository,
        @Autowired ChatMessageRepository chatMessageRepository,
        @Autowired ChatMapper chatMapper,
        @Autowired ChatMessageMapper chatMessageMapper,
        @Autowired ItemService itemService
    ) {
        this.chatRepository = chatRepository;
        this.chatMessageRepository = chatMessageRepository;
        this.chatMapper = chatMapper;
        this.chatMessageMapper = chatMessageMapper;
        this.itemService = itemService;
    }
    
    /**
     * Checks if the user is either the lessor or initiator
     * in a chat, and as such can participate in it
     * @param user the user to be checked for access 
     * @param chat the chat the user's acces is checked against
     * @return true if the user has access, false if not
     */
    private boolean hasAccessToChat(User user, Chat chat) {
        return (
            user.equals(chat.getPrimaryKey().getInitiator()) ||
            user.equals(chat.getPrimaryKey().getItem().getUser())
        );
    }

    /**
     * Creates a chat and saves it to the repository
     * @param chatDto DTO of the chat to be created
     * @param initiator the initiator of the chat
     * @return the created chat as a DTO
     */
    @Transactional
    public ChatDTO createChat(ChatDTO chatDto, User initiator) {
        logger.debug("Creating chat");

        Chat chat = chatMapper.fromDTO(chatDto, initiator);

        if (chatRepository.existsById(chat.getPrimaryKey())) {
            throw new ChatAlreadyExsistsException();
        }

        chat = chatRepository.save(chat);
        
        return chatMapper.toDTO(chat);
    }

    /**
     * Gets all chats a user has partipated in
     * @param user the user to retrieve chats by
     * @return all chats a user has partipated in, as a list
     */
    @Transactional
    public List<ChatDTO> getChats(User user) {
        logger.debug("Getting all chats for user " + user);

        return chatMapper.toDTOs(this.chatRepository.findByUser(user));
    }

    /**
     * Gets a specific chat for a user based on the item it is connected to
     * @param itemId the id of the item
     * @param user the user to get the chat for
     * @return the chat that is connected to both the user and item
     */
    public ChatDTO getChat(long itemId, User user) {
        logger.debug("Getting chat for " + user);

        Item item = itemService.getItemById(itemId);
        ChatPrimaryKey chatPk = new ChatPrimaryKey(item, user);

        return chatMapper.toDTO(
            chatRepository.findById(chatPk)
                .filter(x -> hasAccessToChat(user, x))
                .orElseThrow(ForbiddenException::new)
        );
    }

    /**
     * Creates a chat message and adds it to the repository
     * @param chatUuid the uuid of the chat the message is sent in
     * @param dto the chat message as a DTO
     * @param sender the user sending the message  
     * @return the created chat meessage as a DTO 
     */
    @Transactional
    public ChatMessageDTO createChatMessage(String chatUuid, ChatMessageDTO dto, User sender) {
        logger.debug("Creating chat message");

        Chat chat = chatRepository.findByUuid(UUID.fromString(chatUuid))
            .filter(x -> hasAccessToChat(sender, x))
            .orElseThrow(ForbiddenException::new);

        ChatMessage message = chatMessageMapper.fromDTO(dto, chat, sender);
    
        message = chatMessageRepository.save(message);

        return chatMessageMapper.toDTO(message);
    }

    /**
     * Gets all messages in a chat
     * @param chatUuid the uuid of the chat to retrieve messages from
     * @param user the user attempting to retrieve the messages. User is checked for access
     * @return all messages in a chat, as a list
     */
    @Transactional
    public List<ChatMessageDTO> getMessages(String chatUuid, User user) {
        logger.debug("Getting all messages for chat");

        Chat chat = chatRepository.findByUuid(UUID.fromString(chatUuid))
            .filter(x -> hasAccessToChat(user, x))
            .orElseThrow(ForbiddenException::new);
        
        return chatMessageMapper.toDTOs(chatMessageRepository.getAllMessagesByChat(chat));
    }

}
