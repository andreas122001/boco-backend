package edu.ntnu.idatt2106.bocobackend.service;

import edu.ntnu.idatt2106.bocobackend.dto.ItemDTO;
import edu.ntnu.idatt2106.bocobackend.dto.ItemImageDTO;
import edu.ntnu.idatt2106.bocobackend.dto.ItemsPageDTO;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.ItemImageMapper;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.ItemMapper;
import edu.ntnu.idatt2106.bocobackend.model.Item;
import edu.ntnu.idatt2106.bocobackend.model.ItemImage;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.model.exception.ForbiddenException;
import edu.ntnu.idatt2106.bocobackend.model.exception.ImageDeletionException;
import edu.ntnu.idatt2106.bocobackend.model.exception.ItemImageNotFoundException;
import edu.ntnu.idatt2106.bocobackend.model.exception.ItemNotFoundException;
import edu.ntnu.idatt2106.bocobackend.repository.ItemImageRepository;
import edu.ntnu.idatt2106.bocobackend.repository.ItemQueryParams;
import edu.ntnu.idatt2106.bocobackend.repository.ItemRepository;
import edu.ntnu.idatt2106.bocobackend.util.ItemCategory;
import jakarta.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Handles getting and editing items and adding items to a user.
 */
@Service
public class ItemService {

    private static final Logger logger = LogManager.getLogger(ItemService.class);

    private final ItemRepository itemRepository;
    private final ItemImageRepository itemImageRepository;
    private final ItemImageMapper itemImageMapper;
    private final ItemMapper itemMapper;

    public ItemService(
            @Autowired ItemRepository itemRepository,
            @Autowired ItemImageRepository itemImageRepository,
            @Autowired ItemImageMapper itemImageMapper,
            @Autowired ItemMapper itemMapper) {
        this.itemRepository = itemRepository;
        this.itemImageRepository = itemImageRepository;
        this.itemImageMapper = itemImageMapper;
        this.itemMapper = itemMapper;
    }

    /**
     * Creates an item and saves it to the repository.
     * 
     * @param item the item to create.
     * @param user the user (lessor) creating the item
     * @return created item as a DTO.
     */
    public ItemDTO create(ItemDTO item, User user) {
        logger.debug("Creating item {}", item);

        item.setUserEmail(user.getEmail());
        Item newItem = itemMapper.fromDTO(item, user);

        Item savedItem = itemRepository.save(newItem);
        return itemMapper.toDTO(savedItem);
    }

    /**
     * Gets an item by it's id
     * 
     * @param id the id of the item.
     * @return the item.
     */
    public Item getItemById(long id) {
        Optional<Item> item = itemRepository.findById(id);
        Item foundItem = item.orElseThrow(ItemNotFoundException::new);

        logger.debug("Found item {}", foundItem);
        return foundItem;
    }

    /**
     * Gets an item by id.
     * 
     * @param id of item.
     * @return the item as a DTO.
     */
    @Transactional
    public ItemDTO getById(long id) {
        return itemMapper.toDTO(getItemById(id));
    }

    /**
     * Gets all items of a user by the user's email, paginated
     *Method can also sort, filter and search individually or combined by choice
     * 
     * @param pageNo the number of pages to paginate on
     * @param pageSize the number of items per page
     * @param sortBy the value to sort the items by
     * @param sortDir used to select between ASC and DESC sorting
     * @param search the value to search the items by, default is ""
     * @param category the category to filter the items by
     * @param email email of the user.
     * 
     * @return all items of the user, as a ItemsPageDTO
     */
    public ItemsPageDTO getItems(
        int pageNo,
        int pageSize,
        String sortBy,
        String sortDir,
        String search,
        String email,
        String category
    ) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name())
            ? Sort.by(sortBy).ascending()
            : Sort.by(sortBy).descending();

        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
        ItemCategory itemCategory = category != null ? ItemCategory.valueOf(category.toUpperCase()) : null;
        Page<Item> page = this.itemRepository.findItems(
            new ItemQueryParams()
                .search(search)
                .email(email)
                .category(itemCategory)
                .pageable(pageable)
        );

        List<ItemDTO> content = itemMapper.toDTOs(page.getContent());
        ItemsPageDTO response = new ItemsPageDTO();
        response.setContent(content);
        response.setPageNo(page.getNumber());
        response.setPageSize(page.getSize());
        response.setTotalElements(page.getTotalElements());
        response.setTotalPages(page.getTotalPages());
        response.setLast(page.isLast());

        logger.info("{} items returned", response.getContent().size());
        return response;
    }

    /**
     * Deletes an item from the database by id.
     * 
     * @param authUser the requesting user.
     * @param id       of the item.
     * @return deleted item as a DTO.
     */
    @Transactional
    public ItemDTO deleteById(User authUser, long id) {
        Item foundItem = this.getItemById(id); // throws if not found

        if (!foundItem.getUser().equals(authUser)) {
            throw new ForbiddenException();
        }

        itemRepository.deleteById(id);
        logger.debug("Item '{}' deleted", foundItem);

        return itemMapper.toDTO(foundItem);
    }

    /**
     * Updates an item. User is validated.
     * ID, user and images are ignored.
     * 
     * @param authUser the requesting user.
     * @param id       the id of the item to be updated
     * @param item     the item to be updated
     * @return the updated item as a DTO
     */
    @Transactional
    public ItemDTO update(User authUser, long id, ItemDTO item) {
        Item foundItem = this.getItemById(id); // throws if not found

        if (!foundItem.getUser().equals(authUser)) {
            throw new ForbiddenException();
        }

        itemMapper.updateFromDTO(item, foundItem);

        Item updatedItem = itemRepository.save(foundItem);

        logger.debug("Item '{}' updated", item);
        return itemMapper.toDTO(updatedItem);
    }

    /**
     * Adds an image to an item. User is validated.
     * 
     * @param authUser     the requesting user.
     * @param itemImageDTO the image to add.
     * @param itemId       id of item to add to.
     * @return the added ItemImage as a DTO
     */
    @Transactional
    public ItemImageDTO addImageToItem(User authUser, ItemImageDTO itemImageDTO, long itemId) {
        Item foundItem = getItemById(itemId); // throws if not found
        itemImageDTO.setItemId(itemId);

        if (!foundItem.getUser().equals(authUser)) {
            throw new ForbiddenException();
        }

        logger.debug("Adding image to item");
        ItemImage image = itemImageMapper.fromDTO(itemImageDTO, foundItem);
        ItemImage savedImage = itemImageRepository.save(image);

        return itemImageMapper.toDTO(savedImage);
    }

    /**
     * Gets an image by id.
     * 
     * @param imageId id of image.
     * @return the image as a DTO.
     */
    @Transactional
    public ItemImageDTO getImageById(long imageId) {
        Optional<ItemImage> image = itemImageRepository.findById(imageId);

        return itemImageMapper.toDTO(image.orElseThrow(ItemImageNotFoundException::new));
    }

    /**
     * Removes an image from an item. User is validated.
     * 
     * @param authUser the requesting user.
     * @param imageId  the id of the image to delete.
     * @param itemId   the id of the item which the image belongs to.
     */
    @Transactional
    public void removeImageFromItem(User authUser, long imageId, long itemId) {
        Item foundItem = getItemById(itemId);

        if (!foundItem.getUser().equals(authUser)) {
            throw new ForbiddenException();
        }

        if (!itemImageRepository.existsById(imageId)) {
            throw new ItemImageNotFoundException();
        }

        if(itemImageRepository.findByItem(foundItem).size()-1 == 0){
            throw new ImageDeletionException();
        }

        itemImageRepository.deleteById(imageId);
    }

}
