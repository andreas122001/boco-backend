package edu.ntnu.idatt2106.bocobackend.service;

import edu.ntnu.idatt2106.bocobackend.model.Lease;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.model.Username;
import edu.ntnu.idatt2106.bocobackend.dto.LeaseDTO;
import edu.ntnu.idatt2106.bocobackend.dto.LeasePeriodDTO;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.LeaseMapper;
import edu.ntnu.idatt2106.bocobackend.model.exception.ForbiddenException;
import edu.ntnu.idatt2106.bocobackend.model.exception.ItemLeasedInTimePeriodException;
import edu.ntnu.idatt2106.bocobackend.repository.LeaseRepository;
import edu.ntnu.idatt2106.bocobackend.util.Actor;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Handles getting and editing leases and creating leases.
 */
@Service
public class LeaseService {
    private final LeaseRepository leaseRepository;
    private final ItemService itemService;
    private final UserService userService;
    private final LeaseMapper leaseMapper;

    Logger logger = LoggerFactory.getLogger(LeaseService.class);

    public LeaseService(
        @Autowired LeaseRepository leaseRepository,
        @Autowired ItemService itemService,
        @Autowired UserService userService,
        @Autowired LeaseMapper leaseMapper
    ) {
        this.leaseRepository = leaseRepository;
        this.itemService = itemService;
        this.userService = userService;
        this.leaseMapper = leaseMapper;
    }

    /**
     * Creates a lease from a LeaseDTO.
     * @param actor the user that creates the lease, e.g. an admin
     * @param username the email of the lessee of the item
     * @param leaseDto the dto to create a lease of.
     * @return the created lease as a DTO.
     */
    @Transactional
    public LeaseDTO create(Actor actor, String username, LeaseDTO leaseDto) {
        if(itemIsLeasedInTimePeriod(leaseDto)){ throw new ItemLeasedInTimePeriodException();};
        logger.debug("Creating lease");

        Lease lease = new Lease(
            userService.getUserByUsernameAsActor(actor, new Username(username)),
            itemService.getItemById(leaseDto.getItemId()),
            leaseDto.getStartTime(), 
            leaseDto.getEndTime()
        );
        
        Lease createdLease = this.leaseRepository.save(lease);
        return leaseMapper.toDTO(createdLease);
    }

    /**
     * Gets a lease by id.
     * @param id of lease.
     * @return the lease as a DTO.
     */
    public LeaseDTO getById(Actor actor, String id) {
        logger.debug("Finding lease");
        
        Lease lease = this.leaseRepository.findById(UUID.fromString(id))
            .orElseThrow(() -> {
                logger.debug("Lease " + id + " does not exist");
                return new ForbiddenException();
            });
        
        if (!actor.hasAccessToUsername(new Username(lease.getLessee().getEmail()))) {
            logger.debug("Actor does not have access to lease " + id);
            throw new ForbiddenException();
        }

        return leaseMapper.toDTO(lease);
    } 

    /**
     * Gets all leases by a user
     * @param actor the user that fetches the leases, e.g. an admin
     * @param username the user to find leases by
     * @return all leases by a user, as a list
     */
    @Transactional
    public List<LeaseDTO> getLeasesByUsername(Actor actor, String username) {
        logger.debug("Finding lease(s)");

        User user = userService.getUserByUsernameAsActor(actor, new Username(username));

        return leaseMapper.toDTOs(
            leaseRepository.getLeasesByUser(user)
        );
    }

    /**
     * Deletes a lease by id.
     * @param id id of the lease.
     * @return the deleted lease as a DTO.
     */
    @Transactional
    public LeaseDTO deleteById(Actor actor, String id) {
        logger.debug("Deleting lease");
        
        LeaseDTO lease = this.getById(actor, id); 
        if (!actor.hasAccessToUsername(new Username(lease.getLesseeEmail()))) {
            throw new ForbiddenException();
        }
        this.leaseRepository.deleteById(UUID.fromString(id));
        
        return lease;
    }
    /**
     * Check if item is leased in timeperiod
     * @param leaseDTO
     * @return true if item is leased, false if not
     */
    public boolean itemIsLeasedInTimePeriod(LeaseDTO leaseDTO){
        
       List<Lease> leasesOnItem = leaseRepository.getLeasesByItem(itemService.getItemById(leaseDTO.getItemId()));

       if(leasesOnItem.isEmpty()){return false;}

       for(Lease l : leasesOnItem){
           if(l.getStartTime().isBefore(leaseDTO.getEndTime()) 
           && leaseDTO.getStartTime().isBefore(l.getEndTime())){
            return true;
        }
       }
       return false;
    }

    /**
     * Gets all leases on an item by an item's id
     * @param itemID the id of the item
     * @returns all leases on a item as a list
     */
    public List<LeasePeriodDTO> getAllLeasesByItemID(long itemID){

        List<Lease> leases = leaseRepository.getLeasesByItem(itemService.getItemById(itemID));
        return leaseMapper.toLeasePeriodDTOs(leases);
       }
    }
