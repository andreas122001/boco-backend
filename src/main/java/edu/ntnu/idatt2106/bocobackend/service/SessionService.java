package edu.ntnu.idatt2106.bocobackend.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import edu.ntnu.idatt2106.bocobackend.repository.SessionRepository;
import jakarta.transaction.Transactional;
import edu.ntnu.idatt2106.bocobackend.dto.SessionDTO;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.SessionMapper;
import edu.ntnu.idatt2106.bocobackend.model.Session;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.model.UserCredentials;
import edu.ntnu.idatt2106.bocobackend.model.exception.InvalidCredentialsException;
import edu.ntnu.idatt2106.bocobackend.model.exception.SessionNotFoundException;
import edu.ntnu.idatt2106.bocobackend.model.exception.UserNotFoundException;

/**
 * SessionServices handles creation and deletion of user sessions.
 */
@Service
public class SessionService {
    private final UserService userService;
    private final SessionRepository sessionRepository;
    private final SessionMapper sessionMapper;

    Logger logger = LoggerFactory.getLogger(SessionService.class);

    /**
     * Constructor for SessionService.
     * @param userService UserService that SessionService will use.
     * @param sessionRepository SessionRepository to use for SessionService.
     */
    public SessionService(
        @Autowired UserService userService, 
        @Autowired SessionRepository sessionRepository,
        @Autowired SessionMapper sessionMapper
    ) {
        this.userService = userService;
        this.sessionRepository = sessionRepository;
        this.sessionMapper = sessionMapper;
    }

    /**
     * Creates a session for a user with the specified credentials.
     * @param credentials The user credentials.
     * @return New session for the user.
     * @throws InvalidCredentialsException The credentials were invalid. The username or the password was incorrect.
     */
    public SessionDTO create(UserCredentials credentials) {
        logger.debug("Creating session for user " + credentials.getEmail());

        User user;
        try {
            user = userService.getByEmailAndPassword(credentials.getEmail(), credentials.getPassword());
        } catch (UserNotFoundException e) {
            throw new InvalidCredentialsException();
        }

        Session session = new Session(user);
        sessionRepository.save(session);

        logger.debug("Session created for " + credentials.getEmail());
        SessionDTO sessionDTO = sessionMapper.toDTO(session);

        return sessionDTO;
    }

    /**
     * Finds and refreshes a session by token.
     * @param token The Base64-encoded token of the session to find.
     */
    @Transactional
    public Optional<Session> findAndRefresh(String dtoToken) {
        logger.debug("Finding session");

        byte[] token = sessionMapper.decodeToken(dtoToken);
        Optional<Session> session = sessionRepository.findById(token);
        session.ifPresent(Session::refresh);

        logger.debug(session.isPresent() ? "Session found" : "Session not found");
        
        return session;
    }

    /**
     * Deletes a session.
     * @param token The Base64-encoded token of the session to be deleted.
     * @throws SessionNotFoundException The session with the specified token does not exist.
     */
    public void delete(String dtoToken) {
        logger.debug("Deleting session");
        
        byte[] token = sessionMapper.decodeToken(dtoToken);
        Session session = sessionRepository.findById(token)
            .orElseThrow(() -> { 
                logger.warn("Attempted to delete a non-existent session");
                return new SessionNotFoundException();
            });
        
        sessionRepository.delete(session);

        logger.debug("Session deleted");
    }
    
    /**
     * Purges all expired sessions.
     */
    @Scheduled(fixedDelay = 1000  * 60 * 5) // every 5 minutes
    public void purgeExpired() {
        logger.debug("Cleaning up expired sessions");
        sessionRepository.deleteAllExpired();
    }
}
