package edu.ntnu.idatt2106.bocobackend.service;

import edu.ntnu.idatt2106.bocobackend.model.Address;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.model.UserRole;
import edu.ntnu.idatt2106.bocobackend.model.Username;
import edu.ntnu.idatt2106.bocobackend.dto.ChangePasswordDTO;
import edu.ntnu.idatt2106.bocobackend.dto.UserDTO;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.UserMapper;
import edu.ntnu.idatt2106.bocobackend.model.exception.*;
import edu.ntnu.idatt2106.bocobackend.repository.UserRepository;
import edu.ntnu.idatt2106.bocobackend.util.Actor;
import jakarta.annotation.PostConstruct;
import jakarta.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;

    @Value("${admin.email:}") String adminEmail;
    @Value("${admin.password:}") String adminPassword;

    public UserService(
        @Autowired UserRepository userRepository,
        @Autowired PasswordEncoder passwordEncoder,
        @Autowired UserMapper userMapper
    ) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userMapper = userMapper;
    }

    /**
     * Creates an admin user.
     */
    @PostConstruct
    private void createAdmin() {
        try {
            if (userRepository.existsByEmail(adminEmail)) {
                logger.info("User already exists.");
                return;
            }

            User admin = new User(
                "admin",
                "admin",
                adminEmail,
                passwordEncoder.encode(adminPassword),
                new Address("Admin Street 1", "1337")
            );
            admin.setRole(UserRole.ADMIN);
            userRepository.save(admin);
            logger.info("Created admin user '" + adminEmail + '"');
        } catch (Exception e) {
            logger.error("Unable to create admin user", e);
        }
    }

    /**
     * Creates a new user from user details. Password is hashed.
     * @param userDto user details.
     * @return the new user.
     */
    @Transactional
    public User create(UserDTO userDto) {
        logger.debug("Creating user");

        User user = userMapper.fromDTO(userDto);

        if (this.userRepository.findByEmail(user.getEmail()).isPresent()) throw new UserAlreadyExistsException();

        return this.userRepository.save(user);
    }

    /**
     * Gets a user by email.
     * @param email of user.
     * @return the user.
     */
    public User getByEmail(String email) {
        return this.userRepository.findByEmail(email)
            .orElseThrow(UserNotFoundException::new);
    }

    /**
     * Gets a user by id.
     * @param id of user.
     * @return the user.
     */
    public User getById(long id) {
        return this.userRepository.findById(id)
                .orElseThrow(UserNotFoundException::new);
    }

    /**
     * Gets details of a user. Omits critical information.
     * @param email of target user.
     * @return user details.
     */
    public UserDTO getProfileByEmail(String email) {
        logger.debug("Finding user");
        User user = getByEmail(email);
        return userMapper.toDTO(user);
    }

    /**
     * Gets details of a user. Omits critical information.
     * @param id of target user.
     * @return user details.
     */
    public UserDTO getProfileById(long id) {
        logger.debug("Finding user");
        User user = getById(id);
        return userMapper.toDTO(user);
    }

    /**
     * Gets a user by username (email or 'self')
     * @param actor the requesting user (from token).
     * @param username of user to access (email).
     * @return the user with {@param username}.
     * @throws ForbiddenException The user does not have access to the
     * specified user's account.
     */
    public User getUserByUsernameAsActor(Actor actor, Username username) {
        if (!actor.hasAccessToUsername(username)) {
            logger.warn("Refusing access to get user by username - actor does not have sufficient permissions.");
            throw new ForbiddenException();
        }

        if (username.isSelf()) {
            return actor.getUser();
        }
        String email = username.getUsername();
        return this.getByEmail(email);
    }

    /**
     * Gets a user by email ('username') and password, aka credentials.
     * @param email of user.
     * @param password of password.
     * @return the user.
     */
    public User getByEmailAndPassword(String email, String password) {
        logger.debug("Finding user");

        try {
            User user = getByEmail(email);
            if (!passwordEncoder.matches(password, user.getPasswordHash())) {
                throw new InvalidCredentialsException();
            }
            return user;
        } catch (UserNotFoundException e) {
            throw new InvalidCredentialsException();
        }
    }

    /**
     * Deletes a user by email. Checks if user is owner or admin.
     * @param username of user to be deleted.
     * @return deleted user.
     */
    @Transactional
    public User deleteByUsername(Actor actor, String username) {
        User target = getUserByUsernameAsActor(actor, new Username(username));

        this.userRepository.delete(target);
        logger.debug("User deleted successfully");

        return target;
    }

    /**
     * Sets the password of a user. User is validated.
     * @param username the target user.
     * @param changePasswordDTO dto of password change.
     */
    @Transactional
    public void setPasswordByUsername(Actor actor, String username, ChangePasswordDTO changePasswordDTO) {
        logger.debug("Updating password");
        String oldPassword = changePasswordDTO.getOldPassword();
        String newPassword = changePasswordDTO.getNewPassword();
        User target = getUserByUsernameAsActor(actor, new Username(username));

        if (oldPassword != null && oldPassword.equals(newPassword)) {
            throw new IdenticalPasswordsException();
        }

        if (!actor.getUser().getRole().equals(UserRole.ADMIN)) {
            if (!passwordEncoder.matches(oldPassword, target.getPasswordHash())) {
                throw new InvalidCredentialsException();
            }
        }

        target.setPasswordHash(passwordEncoder.encode(newPassword));

        this.userRepository.save(target);
    }

    /**
     * Updates the username (email) of a user. User is validated.
     * @param actor the acting user (from token).
     * @param username the target users username.
     * @param userDto updated details of user.
     * @return the updated user.
     */
    @Transactional
    public User updateUserByUsername(Actor actor, String username, UserDTO userDto) {
        logger.debug("Updating user");

        User target = getUserByUsernameAsActor(actor, new Username(username));
        userMapper.updateFromDTO(userDto, target);

        return this.userRepository.save(target);
    }
}
