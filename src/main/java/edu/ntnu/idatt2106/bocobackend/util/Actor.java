package edu.ntnu.idatt2106.bocobackend.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.model.UserRole;
import edu.ntnu.idatt2106.bocobackend.model.Username;
import edu.ntnu.idatt2106.bocobackend.model.exception.ForbiddenException;
import edu.ntnu.idatt2106.bocobackend.model.exception.UserUnauthorizedException;
import edu.ntnu.idatt2106.bocobackend.security.SessionAuthenticationToken;

import static java.util.Objects.requireNonNull;

/**
 * User that is performing a request.
 */
public class Actor {
    private final User user;

    public Actor(User user) {
        this.user = requireNonNull(user, "user cannot be null");
    }
    

    /**
     * Gets the actor that performs the current action.
     * @return Actor that performs the current action.
     * @throws ForbiddenException The actor is not a user.
     */
    public static Actor getCurrent() {
        Authentication springAuth = SecurityContextHolder.getContext().getAuthentication();
        if (springAuth == null || !(springAuth instanceof SessionAuthenticationToken)) {
            throw new UserUnauthorizedException();
        }


        SessionAuthenticationToken authentication = (SessionAuthenticationToken)(springAuth);
        return new Actor(authentication.getPrincipal());
    }

    /**
     * Gets the actor user.
     * @return
     */
    public User getUser() {
        return this.user;
    }

    /**
     * Returns whether the actor has access to a username.
     * @param username Username to check access for.
     * @return Whether or not the actor has access to the username.
     */
    public boolean hasAccessToUsername(Username username) {
        if (username.isSelf()) {
            return true;
        }

        boolean isSelf = user.getEmail().equals(username.getUsername());
        boolean isAdmin = user.getRole().equals(UserRole.ADMIN);

        return isSelf || isAdmin;
    }
}
