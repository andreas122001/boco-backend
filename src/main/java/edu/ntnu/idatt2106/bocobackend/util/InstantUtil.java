package edu.ntnu.idatt2106.bocobackend.util;

import java.time.Instant;
import  java.util.Objects;

public class InstantUtil {
    
    public static Instant startTimeChronological(Instant startTime, Instant endtime){
        Objects.requireNonNull(startTime, "startTime cannot be null");

        int value = endtime.compareTo(startTime);
  
        if (value > 0){
            return startTime;
        }
        else if (value == 0){
            throw new IllegalArgumentException("Start and EndTime can not be equal");
        }
        else{
            throw new IllegalArgumentException("Start time can cannot be after endtime");
        }
            
    }
    public static Instant endTimeChronological(Instant startTime, Instant endtime){
        Objects.requireNonNull(startTime, " cannot be null");

        int value = startTime.compareTo(endtime);
  
        if (value < 0){
            return endtime;
        }
        else if (value == 0){
            throw new IllegalArgumentException("Start and EndTime can not be equal");
        }
        else{
            throw new IllegalArgumentException("StartTime cannot  be after endtime");
        }
            
    }
}
