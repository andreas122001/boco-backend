package edu.ntnu.idatt2106.bocobackend.util;

/**
 * Categories that items can have.
 */
public enum ItemCategory {
    OTHER,
    NATURE,
    GARDEN,
    ART,
    SPORTS,
    PETS,
    KIDS,
    MOTOR;
}