package edu.ntnu.idatt2106.bocobackend.util;

public class PaginationConstants {

    public static final String DEFAULT_PAGE_NUMBER = "0";
    public static final String DEFAULT_PAGE_SIZE = "2147483647"; // Integer.MAX_VALUE
    public static final String DEFAULT_SORT_BY = "id";
    public static final String DEFAULT_SORT_DIRECTION = "asc";
}
