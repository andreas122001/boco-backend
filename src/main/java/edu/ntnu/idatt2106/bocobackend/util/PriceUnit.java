package edu.ntnu.idatt2106.bocobackend.util;

public enum PriceUnit {
    HOUR,
    DAY,
    WEEK,
    MONTH
}
