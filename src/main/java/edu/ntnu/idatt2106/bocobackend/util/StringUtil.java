package edu.ntnu.idatt2106.bocobackend.util;

import java.util.Objects;

public class StringUtil {
    public static String trimAndThrowIfNullOrBlank(String str, String name) {
        Objects.requireNonNull(str, name + " cannot be null");

        str = str.trim();
        if (str.isBlank()) {
            throw new IllegalArgumentException(name + " cannot be blank");
        }

        return str;
    }

    public static String trimAndThrowIfNullOrBlank(String str) {
        return trimAndThrowIfNullOrBlank(str, "str");
    }
}
