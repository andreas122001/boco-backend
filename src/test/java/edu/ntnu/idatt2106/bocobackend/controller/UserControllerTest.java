package edu.ntnu.idatt2106.bocobackend.controller;

import edu.ntnu.idatt2106.bocobackend.dto.AddressDTO;
import edu.ntnu.idatt2106.bocobackend.dto.UserDTO;
import edu.ntnu.idatt2106.bocobackend.model.Address;
import edu.ntnu.idatt2106.bocobackend.model.Session;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.model.exception.UserNotFoundException;
import edu.ntnu.idatt2106.bocobackend.security.AuthenticationExceptionEntryPoint;
import edu.ntnu.idatt2106.bocobackend.security.AuthenticationRequestFilter;
import edu.ntnu.idatt2106.bocobackend.security.SecurityConfig;
import edu.ntnu.idatt2106.bocobackend.service.SessionService;
import edu.ntnu.idatt2106.bocobackend.service.UserService;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Base64;
import java.util.Optional;

import static edu.ntnu.idatt2106.bocobackend.util.TestUtil.asJsonString;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
@ContextConfiguration(classes = {
		UserController.class,
		AuthenticationRequestFilter.class,
		AuthenticationExceptionEntryPoint.class,
		SecurityConfig.class
})
public class UserControllerTest {
	@Resource
	private MockMvc mockMvc;

	@MockBean
	private SessionService sessionService;

	@MockBean
	private UserService userService;

	private User exampleUser;
	private UserDTO exampleUserDTO;

	@BeforeEach
	void setup() {
		exampleUser = new User("Kåre", "Erikson", "kåre@ntnu.no","123",
				new Address("O. S. Bragstads Plass", "7034"));
		exampleUserDTO = new UserDTO("Kåre", "Erikson", "kåre@ntnu.no",null,
				new AddressDTO("O. S. Bragstads Plass", "7034"));
	}

	String mockedBearerToken(User user) {
		Session session = new Session(user);
		String token = Base64.getUrlEncoder().encodeToString(session.getToken());
		String header = "Bearer " + token;

		when(sessionService.findAndRefresh(eq(token))).thenReturn(Optional.of(session));

		return header;
	}

	@Test
	void getCurrentUser() throws Exception {
	   	this.mockMvc.perform(get("/users/self")
		   				.header(HttpHeaders.AUTHORIZATION, mockedBearerToken(exampleUser))
						.contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON))
				.andExpectAll(
						status().isOk(),
						jsonPath("$").exists(),
						jsonPath("$.id", is(0)),
						jsonPath("$.firstName", is(exampleUser.getFirstName())),
						jsonPath("$.lastName", is(exampleUser.getLastName()))
				);
	}

	@Test
	@DisplayName("Updating password returns void and 'no content'")
	void updatePassword_returnsVoidAndNoContent() throws Exception {
		this.mockMvc.perform(put("/users/"+exampleUser.getEmail()+"/password")
						.header(HttpHeaders.AUTHORIZATION, mockedBearerToken(exampleUser))
						.content("{\"oldPassword\":\"123\", \"newPassword\":\"1234\"}")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").doesNotExist())
				.andExpect(status().isNoContent());
	}

	@Test
	@DisplayName("Deleting user returns user and 'ok'")
	void deleteUser_returnsUserAndOK() throws Exception {
		when(userService.deleteByUsername(
			any(),
			eq(exampleUser.getEmail())
		)).thenReturn(exampleUser);

		this.mockMvc.perform(delete("/users/"+ exampleUser.getEmail())
					.header(HttpHeaders.AUTHORIZATION, mockedBearerToken(exampleUser)))
				.andExpect(jsonPath("$").exists());
	}

	@Test
	@DisplayName("Getting user returns the user and OK")
	void onGetExistingUser_returnsUserAndOK() throws Exception {

		when(userService.getProfileByEmail(eq(exampleUser.getEmail()))).thenReturn(exampleUserDTO);

		this.mockMvc.perform(get("/users/"+exampleUser.getEmail())
						.header(HttpHeaders.AUTHORIZATION, mockedBearerToken(exampleUser))
						.contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON))
				.andExpectAll(
						status().isOk(),
						jsonPath("$").exists(),
						jsonPath("$.email", is(exampleUser.getEmail())),
						jsonPath("$.firstName", is(exampleUser.getFirstName())),
						jsonPath("$.lastName", is(exampleUser.getLastName()))
				);
	}

	@Test
	@DisplayName("Getting non-existent user returns 'not found'")
	void onNonGetExistentUser_returnsNotFound() throws Exception {

		when(userService.getProfileByEmail(anyString()))
				.thenThrow(UserNotFoundException.class);

		this.mockMvc.perform(get("/users/"+"foo@bar.com")
						.header(HttpHeaders.AUTHORIZATION, mockedBearerToken(exampleUser))
						.contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON))
				.andExpectAll(
						status().isNotFound(),
						jsonPath("$").doesNotExist()
				);
	}

	@Test
	@DisplayName("Creating new user returns the user and 'created'")
	void creatingUser_returnsUserAndCreated() throws Exception {

		when(userService.create(exampleUserDTO)).thenReturn(exampleUser);

		this.mockMvc.perform(post("/users")
						.header(HttpHeaders.AUTHORIZATION, mockedBearerToken(exampleUser))
						.content(asJsonString(exampleUserDTO))
						.contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON))
				.andExpectAll(
						status().isCreated(),
						jsonPath("$").exists(),
						jsonPath("$.id", is(0)),
						jsonPath("$.firstName", is(exampleUser.getFirstName())),
						jsonPath("$.lastName", is(exampleUser.getLastName()))
				);
	}

	@Test
	@DisplayName("Updating a user returns the new user and 'ok'")
	void puttingUser_returnsNewUser() throws Exception {
		exampleUserDTO.setFirstName("Peter");

		when(userService.updateUserByUsername(
			any(),
			eq(exampleUserDTO.getEmail()),
			eq(exampleUserDTO)
		)).thenReturn(exampleUser);

		this.mockMvc.perform(put("/users/" + exampleUser.getEmail())
						.header(HttpHeaders.AUTHORIZATION, mockedBearerToken(exampleUser))
						.content(asJsonString(exampleUserDTO))
						.contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON))
				.andExpectAll(
						status().isOk(),
						jsonPath("$").exists(),
						jsonPath("$.id", is(0)),
						jsonPath("$.firstName", is(exampleUser.getFirstName())),
						jsonPath("$.lastName", is(exampleUser.getLastName()))
				);
	}
}
