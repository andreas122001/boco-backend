package edu.ntnu.idatt2106.bocobackend.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

public class AddressTest {
    @Test
    void constructor_validArguments_doesNotThrow() {
        new Address("Old town road 2", "7040");
    }

    @ParameterizedTest
    @ArgumentsSource(ConstructorInvalidArgumentsProvider.class)
    void constructor_invalidArguments_throwsException(
        String street, 
        String postalCode,
        Class<Throwable> throwable
    ) {
        assertThrows(throwable, () -> new Address(street, postalCode));
    }


    @ParameterizedTest
    @ArgumentsSource(NullAndEmptyArgumentsProvider.class)
    void setStreet_invalidStreet_throwsException(String street, Class<Throwable> exception) {
        Address address = new Address("Old town road 2", "1337");

        assertThrows(exception, () -> address.setStreet(street));
    }

    @Test
    void setStreet_nonBlankStreet_setsStreet() {
        Address address = new Address("Old town road 2", "1337");

        address.setStreet("Old town road 3");

        assertEquals("Old town road 3", address.getStreet());
    }


    @ParameterizedTest
    @ArgumentsSource(InvalidPostalCodeArgumentsProvider.class)
    void setPostalCode_invalidPostalCode_throwsException(String postalCode) {
        Address address = new Address("Old town road 2", "1337");

        assertThrows(IllegalArgumentException.class, () -> address.setPostalCode(postalCode));
    }

    @Test
    void setPostalCode_nonZeroOrNegative_setsPostalCode() {
        Address address = new Address("Old town road 2", "1337");

        address.setPostalCode("2000");

        assertEquals("2000", address.getPostalCode());
    }

    static class InvalidPostalCodeArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
            return Stream.of(
                Arguments.of("0"),
                Arguments.of("-1"),
                Arguments.of("28582")
            );
        }
    }

    static class NullAndEmptyArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
            return Stream.of(
                Arguments.of((String)null, NullPointerException.class),
                Arguments.of("", IllegalArgumentException.class),
                Arguments.of("  ", IllegalArgumentException.class)
            );
        }
    }

    static class ConstructorInvalidArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext arg0) throws Exception {
            return Stream.of(
                Arguments.of(null, "1337", NullPointerException.class),
                Arguments.of("", "1337", IllegalArgumentException.class),
                Arguments.of("  ", "1337", IllegalArgumentException.class),

                Arguments.of("Old town road 2", "0", IllegalArgumentException.class),
                Arguments.of("Old town road 2", "29488448", IllegalArgumentException.class)
            );
        }
    }
}
