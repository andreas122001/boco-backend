package edu.ntnu.idatt2106.bocobackend.model;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Instant;
import java.util.stream.Stream;

import edu.ntnu.idatt2106.bocobackend.util.ItemCategory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

import edu.ntnu.idatt2106.bocobackend.util.PriceUnit;

public class ChatMessageTest {

    @Test
    void constructor_validArguments_doesNotThrow() {
        User lessor = new User("Name", "Nameson", "name@example.com", "93tjgag%G%$$$$", new Address());
        User lessee = new User("Jame", "Jameson", "example@name.com", "93tjgag%G%$$$$", new Address());
        Item item = new Item("IT developer", 150, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now().minusSeconds(150), Instant.now().plusSeconds(150), "Cheap dev available for hire", lessor);
        ChatPrimaryKey chatId = new ChatPrimaryKey(item, lessee);
        Chat chat = new Chat(chatId);

        assertDoesNotThrow(() -> new ChatMessage(Instant.now(), "Hi I'd like to rent you hammer", chat, lessee));
    }

    @ParameterizedTest
    @ArgumentsSource(ConstructorInvalidArgumentsProvider.class)
    void constructor_invalidArguments_throwsException(
        Instant timestamp, 
        String message, 
        Chat chat, 
        User sender,
        Class<Throwable> type
    ) {
        assertThrows(type, () -> new ChatMessage(timestamp, message, chat, sender));
    }

    @Test
    void getTimestamp_returnsTimestamp() {
        User lessor = new User("Name", "Nameson", "name@example.com", "93tjgag%G%$$$$", new Address());
        User lessee = new User("Jame", "Jameson", "example@name.com", "93tjgag%G%$$$$", new Address());
        Item item = new Item("IT developer", 150, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now().minusSeconds(150), Instant.now().plusSeconds(150), "Cheap dev available for hire", lessor);
        ChatPrimaryKey chatId = new ChatPrimaryKey(item, lessee);
        Chat chat = new Chat(chatId);
        Instant timestamp = Instant.now();

        ChatMessage chatMessage = new ChatMessage(timestamp, "Hi I'd like to rent you hammer", chat, lessee);
        
        assertEquals(timestamp, chatMessage.getTimestamp());
    }

    @Test
    void setTimestap_nonNullTimestamp_updatesTimestamp() {
        User lessor = new User("Name", "Nameson", "name@example.com", "93tjgag%G%$$$$", new Address());
        User lessee = new User("Jame", "Jameson", "example@name.com", "93tjgag%G%$$$$", new Address());
        Item item = new Item("IT developer", 150, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now().minusSeconds(150), Instant.now().plusSeconds(150), "Cheap dev available for hire", lessor);
        ChatPrimaryKey chatId = new ChatPrimaryKey(item, lessee);
        Chat chat = new Chat(chatId);

        Instant timestamp = Instant.now();
        Instant newTimestamp = Instant.now().plusSeconds(100);
        ChatMessage chatMessage = new ChatMessage(timestamp, "Hi I'd like to rent you hammer", chat, lessee);
        
        chatMessage.setTimestamp(newTimestamp);

        assertEquals(newTimestamp, chatMessage.getTimestamp());
    }

    @Test
    void setTimestap_invalidTimestamp_throwsException() {
        User lessor = new User("Name", "Nameson", "name@example.com", "93tjgag%G%$$$$", new Address());
        User lessee = new User("Jame", "Jameson", "example@name.com", "93tjgag%G%$$$$", new Address());
        Item item = new Item("IT developer", 150, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now().minusSeconds(150), Instant.now().plusSeconds(150), "Cheap dev available for hire", lessor);
        ChatPrimaryKey chatId = new ChatPrimaryKey(item, lessee);
        Chat chat = new Chat(chatId);

        Instant timestamp = Instant.now();
        ChatMessage chatMessage = new ChatMessage(timestamp, "Hi I'd like to rent you hammer", chat, lessee);
        
        assertThrows(
            NullPointerException.class,
            ()-> chatMessage.setTimestamp(null)
        );
    }

    @Test
    void getMessage_returnsMessage() {
        User lessor = new User("Name", "Nameson", "name@example.com", "93tjgag%G%$$$$", new Address());
        User lessee = new User("Jame", "Jameson", "example@name.com", "93tjgag%G%$$$$", new Address());
        Item item = new Item("IT developer", 150, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now().minusSeconds(150), Instant.now().plusSeconds(150), "Cheap dev available for hire", lessor);
        ChatPrimaryKey chatId = new ChatPrimaryKey(item, lessee);
        Chat chat = new Chat(chatId);

        String message = "Iesus Nazarenus Rex Iudaeorum";
        ChatMessage chatMessage = new ChatMessage(Instant.now(), message, chat, lessee);

        assertEquals(message, chatMessage.getContent());
    }

    @Test
    void setMessage_nonNullMessage_updatesMessage() {
        User lessor = new User("Name", "Nameson", "name@example.com", "93tjgag%G%$$$$", new Address());
        User lessee = new User("Jame", "Jameson", "example@name.com", "93tjgag%G%$$$$", new Address());
        Item item = new Item("IT developer", 150, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now().minusSeconds(150), Instant.now().plusSeconds(150), "Cheap dev available for hire", lessor);
        ChatPrimaryKey chatId = new ChatPrimaryKey(item, lessee);
        Chat chat = new Chat(chatId);

        String message = "Whazzup babyyy";
        String newMessage = "Greetings my Lady";
        ChatMessage chatMessage = new ChatMessage(Instant.now(), message, chat, lessee);
        
        chatMessage.setContent(newMessage);

        assertEquals(newMessage, chatMessage.getContent());
    }

    @Test
    void setMessage_nullMessage_throwsException() {
        User lessor = new User("Name", "Nameson", "name@example.com", "93tjgag%G%$$$$", new Address());
        User lessee = new User("Jame", "Jameson", "example@name.com", "93tjgag%G%$$$$", new Address());
        Item item = new Item("IT developer", 150, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now().minusSeconds(150), Instant.now().plusSeconds(150), "Cheap dev available for hire", lessor);
        ChatPrimaryKey chatId = new ChatPrimaryKey(item, lessee);
        Chat chat = new Chat(chatId);

        ChatMessage chatMessage = new ChatMessage(Instant.now(), "Judgement Day will come!", chat, lessee);

        assertThrows(
            NullPointerException.class,
            () -> chatMessage.setContent(null)
        );
    }

    @Test
    void setMessage_emptyMessage_throwsException() {
        User lessor = new User("Name", "Nameson", "name@example.com", "93tjgag%G%$$$$", new Address());
        User lessee = new User("Jame", "Jameson", "example@name.com", "93tjgag%G%$$$$", new Address());
        Item item = new Item("IT developer", 150, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now().minusSeconds(150), Instant.now().plusSeconds(150), "Cheap dev available for hire", lessor);
        ChatPrimaryKey chatId = new ChatPrimaryKey(item, lessee);
        Chat chat = new Chat(chatId);

        ChatMessage chatMessage = new ChatMessage(Instant.now(), "Judgement Day will come!", chat, lessee);

        assertThrows(
            IllegalArgumentException.class,
            () -> chatMessage.setContent("  ")
        );
    }

    @Test
    void getChat_returnsChat() {
        User lessor = new User("Name", "Nameson", "name@example.com", "93tjgag%G%$$$$", new Address());
        User lessee = new User("Jame", "Jameson", "example@name.com", "93tjgag%G%$$$$", new Address());
        Item item = new Item("IT developer", 150, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now().minusSeconds(150), Instant.now().plusSeconds(150), "Cheap dev available for hire", lessor);
        ChatPrimaryKey chatId = new ChatPrimaryKey(item, lessee);
        Chat chat = new Chat(chatId);

        ChatMessage chatMessage = new ChatMessage(Instant.now(), "Judgement Day will come!", chat, lessee);

        assertEquals(chat, chatMessage.getChat());
    }

    @Test
    void getSender_returnsSender() {
        User lessor = new User("Name", "Nameson", "name@example.com", "93tjgag%G%$$$$", new Address());
        User lessee = new User("Jame", "Jameson", "example@name.com", "93tjgag%G%$$$$", new Address());
        Item item = new Item("IT developer", 150, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now().minusSeconds(150), Instant.now().plusSeconds(150), "Cheap dev available for hire", lessor);
        ChatPrimaryKey chatId = new ChatPrimaryKey(item, lessee);
        Chat chat = new Chat(chatId);

        ChatMessage chatMessage = new ChatMessage(Instant.now(), "Judgement Day will come!", chat, lessee);

        assertEquals(lessee, chatMessage.getSender());
    }

    static class ConstructorInvalidArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext arg0) throws Exception {
            return Stream.of(
                Arguments.of(null, "Message text", new Chat(), new User(), NullPointerException.class),
                Arguments.of(Instant.now(), null, new Chat(), new User(), NullPointerException.class),
                Arguments.of(Instant.now(), "Message text", null, new User(), NullPointerException.class),
                Arguments.of(Instant.now(), "Message text", new Chat(), null, NullPointerException.class),

                Arguments.of(Instant.now(), "  ", new Chat(), new User(), IllegalArgumentException.class)
            );
        }
    }
    
}
