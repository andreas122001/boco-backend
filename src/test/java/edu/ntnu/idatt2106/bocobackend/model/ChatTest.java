package edu.ntnu.idatt2106.bocobackend.model;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Instant;

import edu.ntnu.idatt2106.bocobackend.util.ItemCategory;
import org.junit.jupiter.api.Test;

import edu.ntnu.idatt2106.bocobackend.util.PriceUnit;

public class ChatTest {

    @Test
    void constructor_validArguments_doesNotThrow() {
        User user = new User("Name", "Nameson", "name@example.com", "93tjgag%G%$$$$", new Address());
        Item item = new Item("IT developer", 150, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now().minusSeconds(150), Instant.now().plusSeconds(150), "Cheap dev available for hire", user);
        ChatPrimaryKey chatId = new ChatPrimaryKey(item, user);
        
        assertDoesNotThrow(
            () -> new Chat(chatId)
        );
    }

    @Test
    void constructor_invalidArguments_throwsException() {
        assertThrows(
            NullPointerException.class,
            () -> new Chat(null)
        );
    }

    @Test
    void getItem_returnsItem() {
        User user = new User("Name", "Nameson", "name@example.com", "93tjgag%G%$$$$", new Address());
        Item item = new Item("IT developer", 150, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now().minusSeconds(150), Instant.now().plusSeconds(150), "Cheap dev available for hire", user);
        ChatPrimaryKey chatId = new ChatPrimaryKey(item, user);

        Chat chat = new Chat(chatId);

        assertEquals(item, chat.getPrimaryKey().getItem());
    }
    
}