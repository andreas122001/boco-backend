package edu.ntnu.idatt2106.bocobackend.model;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

public class ItemImageTest {
    
    byte [] data = {1,2,3,4,5,6,7,8,9,0};
    Item item = new Item();
    

    @Test 
    void createItemImage_valid_doseNotThrow(){
        new ItemImage("snus", data, "image/png", item);
    }

    @Test
    void setDescription_null_throws(){
        ItemImage itemimage = new ItemImage("snus", data, "image/png", item);
        assertThrows(IllegalArgumentException.class, () -> itemimage.setDescription(""));
    }

    @Test
    void setData_null_throws(){
        ItemImage itemimage = new ItemImage("snus", data, "image/png", item);
        assertThrows(NullPointerException.class, () -> itemimage.setData(null));
    }

    @Test
    void setItem_null_throws(){
        ItemImage itemimage = new ItemImage("snus", data, "image/png", item);
        assertThrows(NullPointerException.class, () -> itemimage.setItem(null));
    }


    @ParameterizedTest
    @ArgumentsSource(NullArgumentsProvider.class)
    void constructor_nullArguments_throwsNullPointerException(
        String description,
        byte[] data,
        Item item
    ) {
        assertThrows(NullPointerException.class, () -> new ItemImage(description, data, "image/png", item ));
    }

    static class NullArgumentsProvider implements ArgumentsProvider {
        byte[]qwer={1,2,3,4};
        Item item = new Item();
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
            return Stream.of(
                Arguments.of("qwer",qwer,null),
                Arguments.of("qwer",null,item),
                Arguments.of(null,qwer,item)
            );
        }
    }
}
