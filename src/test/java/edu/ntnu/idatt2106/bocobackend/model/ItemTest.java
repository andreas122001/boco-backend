package edu.ntnu.idatt2106.bocobackend.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import java.time.Instant;
import java.util.stream.Stream;

import edu.ntnu.idatt2106.bocobackend.util.ItemCategory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import edu.ntnu.idatt2106.bocobackend.model.ItemTest.NullAndEmptyArgumentsProvider.ConstructorInvalidArgumentsProvider;
import edu.ntnu.idatt2106.bocobackend.util.PriceUnit;

public class ItemTest {
    User user = new User();
    Instant futureTime = Instant.parse("2025-08-18T17:19:28.115Z");
    @Test
    void constructor_validArguments_doesNotThrow(){
        new Item("sko", 100, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now(),futureTime, " uguiku",user);
    }
    
    @Test
    void setPrice_illegalPrice_throwsException() {
       Item item = new Item("title", 100, PriceUnit.HOUR, ItemCategory.GARDEN, Instant.now(), futureTime, "description",user);

        assertThrows(IllegalArgumentException.class, () -> item.setPrice(-1));
    }


    @Test
    void setTitle_noNull_newTitle() {
        Item item = new Item("title", 100, PriceUnit.HOUR, ItemCategory.GARDEN, Instant.now(), futureTime, "description",user);

        item.setTitle("newTitle");
        
        assertEquals("newTitle",item.getTitle());
    }

    @Test
    void setDescription_nonNull_newDescription(){
        Item item = new Item("title", 100, PriceUnit.HOUR, ItemCategory.GARDEN, Instant.now(), futureTime, "description",user);

        item.setDescription("abc");
        
        assertEquals("abc",item.getDescription());
    }
    @Test
    void setEndTime_invalid_throws(){
        Item item = new Item("brandon", 666,PriceUnit.WEEK,ItemCategory.GARDEN, Instant.parse("2000-11-18T17:19:28.115Z"),Instant.now(),"://",user);
       assertThrows(IllegalArgumentException.class, () ->  item.setEndTime(Instant.parse("1942-11-18T17:19:28.115Z")));
    }

    @Test
    void setStartime_invalid_throws(){
        Item item = new Item("obomo", 666,PriceUnit.WEEK,ItemCategory.GARDEN,Instant.parse("1961-08-04T17:19:28.115Z"),Instant.now(),"borock",user);
       assertThrows(IllegalArgumentException.class, () ->  item.setStartTime(Instant.parse("3000-11-18T17:19:28.115Z")));
    }


    @ParameterizedTest
    @ArgumentsSource(NullAndEmptyArgumentsProvider.class)
    void setTitle_invalidTitle_throwsException(String title, Class<Throwable> exception) {
        Item item = new Item("title", 100, PriceUnit.HOUR,ItemCategory.GARDEN,Instant.now(), futureTime, "description",user);

        assertThrows(exception, () -> item.setTitle(title));
    }
    
    @ParameterizedTest
    @ArgumentsSource(ConstructorInvalidArgumentsProvider.class)
    void constructor_invalidArguments_throwsException(
        String title, 
        int price , 
        PriceUnit priceUnit,
        ItemCategory category,
        Instant now, 
        Instant later, 
        String description, 
        User user,
        Class<Throwable> type
    ) {
        assertThrows(type, () -> new Item(title, price,priceUnit,category,now,later,description,user));
    }


    static class NullAndEmptyArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
            return Stream.of(
                Arguments.of((String)null, NullPointerException.class),
                Arguments.of("", IllegalArgumentException.class),
                Arguments.of("  ", IllegalArgumentException.class)
            );
        }
    
        static class ConstructorInvalidArgumentsProvider implements ArgumentsProvider {
            User user;
            Instant futureTime = Instant.parse("2025-08-18T17:19:28.115Z");
            @Override
            public Stream<? extends Arguments> provideArguments(ExtensionContext arg0) throws Exception {
                return Stream.of(
                    Arguments.of(null, 100, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now(),futureTime, " uguiku",user,NullPointerException.class),
                    Arguments.of("snus", 199, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now(),futureTime, null,user,NullPointerException.class),
                    Arguments.of("snus", 199, PriceUnit.DAY, ItemCategory.GARDEN, null,futureTime, "wert",user,NullPointerException.class),
                    Arguments.of("snus", 199, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now(),null, "qwer",user,NullPointerException.class),
                    Arguments.of("snus", 199, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now(),futureTime, null,user,NullPointerException.class),

                    Arguments.of("snus",-20, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now(),futureTime, " uguiku",user,IllegalArgumentException.class)
                );
            }
        }

}
}
