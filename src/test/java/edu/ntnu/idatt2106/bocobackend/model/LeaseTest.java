package edu.ntnu.idatt2106.bocobackend.model;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Instant;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

public class LeaseTest {

    @Test
    void constructor_validArguments_doesNotThrow() {
        assertDoesNotThrow(() -> new Lease(
            new User(),
            new Item(),
            Instant.now(),
            Instant.now().plusSeconds(1000)
        ));
    }

    @Test
    void constructor_startTimeIsAfterEndTime_throwsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> new Lease(
            new User(),
            new Item(),
            Instant.now().plusSeconds(1000),
            Instant.now()
        ));
    }

    @ParameterizedTest
    @ArgumentsSource(NullArgumentsProvider.class)
    void constructor_nullArguments_throwsNullPointerException(
        User lessee,
        Item item,
        Instant startTime,
        Instant endTime
    ) {
        assertThrows(NullPointerException.class, () -> new Lease(lessee, item, startTime, endTime));
    }

    static class NullArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
            return Stream.of(
                Arguments.of(null, new Item(), Instant.now(), Instant.now().plusSeconds(1000)),
                Arguments.of(new User(), null, Instant.now(), Instant.now().plusSeconds(1000)),
                Arguments.of(new User(), new Item(), null, Instant.now().plusSeconds(1000)),
                Arguments.of(new User(), new Item(), Instant.now(), null)
            );
        }
    }
}
