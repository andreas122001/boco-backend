package edu.ntnu.idatt2106.bocobackend.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mockStatic;

import java.time.Instant;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

public class SessionTest {
    @Test
    void constructor_nullUser_throwsNullPointerException() {
        assertThrows(NullPointerException.class, () -> new Session(null));
    }

    @Test
    void constructor_nonNullUser_DoesNotThrow() {
        new Session(new User(
            "Abel", 
            "Born",
            "e@example.com",
            "%¤#/&%¤/",
            new Address("somestreet", "6543")
        ));
    }

    @Test
    void getToken_returnsToken() {
        Session session = new Session(new User(
            "Abel", 
            "Born",
            "e@example.com",
            "%¤#/&%¤/",
            new Address("somestreet", "6543")
        ));

        byte[] token = session.getToken();

        assertEquals(Session.TOKEN_SIZE, token.length);
    }

    @Test
    void isExpired_sessionIdleExpired_returnsTrue() {       
        try (MockedStatic<Instant> instantMock = mockStatic(Instant.class, Mockito.CALLS_REAL_METHODS)) {
            AtomicReference<Instant> now = new AtomicReference<>(Instant.MIN);
            instantMock.when(Instant::now).thenAnswer((x) -> now.get());
            Session session = new Session(new User(
                "Abel", 
                "Born",
                "e@example.com",
                "%¤#/&%¤/",
                new Address("somestreet", "6543")
            ));

            now.set(now.get().plus(Session.IDLE_EXPIRY_DURATION));
            boolean expired = session.isExpired();

            assertTrue(expired);
        }
    }

    @Test
    void refresh_resetsIdleExpiry() {       
        try (MockedStatic<Instant> instantMock = mockStatic(Instant.class, Mockito.CALLS_REAL_METHODS)) {
            AtomicReference<Instant> now = new AtomicReference<>(Instant.MIN);
            instantMock.when(Instant::now).thenAnswer((x) -> now.get());
            Session session = new Session(new User(
                "Abel", 
                "Born",
                "e@example.com",
                "%¤#/&%¤/",
                new Address("somestreet", "6547")
            ));

            now.set(now.get().plus(Session.IDLE_EXPIRY_DURATION));
            session.refresh();
            boolean expired = session.isExpired();

            assertFalse(expired);
        }
    }

    @Test
    void isExpired_sessionAbsoluteExpired_returnsTrue() {
        try (MockedStatic<Instant> instantMock = mockStatic(Instant.class, Mockito.CALLS_REAL_METHODS)) {
            AtomicReference<Instant> now = new AtomicReference<>(Instant.MIN);
            instantMock.when(Instant::now).thenAnswer((x) -> now.get());
            Session session = new Session(new User(
                "Abel", 
                "Born",
                "e@example.com",
                "%¤#/&%¤/",
                new Address("somestreet", "6547")
            ));

            now.set(now.get().plus(Session.ABSOLUTE_EXPIRY_DURATION));
            session.refresh();
            boolean expired = session.isExpired();

            assertTrue(expired);
        }
    }

    @Test
    void isExpired_neitherIdleNorAbsoluteExpired_returnsFalse() {
        Session session = new Session(new User(
            "Abel", 
            "Born",
            "e@example.com",
            "%¤#/&%¤/",
            new Address("somestreet", "6547")
        ));

        boolean expired = session.isExpired();

        assertFalse(expired);
    }
}
