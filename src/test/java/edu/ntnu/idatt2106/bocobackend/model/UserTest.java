package edu.ntnu.idatt2106.bocobackend.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

public class UserTest {
    @Test
    void constructor_validArguments_doesNotThrow() {
        new User("Name", "Nameson", "name@example.com", "93tjgag%G%$$$$", new Address());
    }

    @ParameterizedTest
    @ArgumentsSource(ConstructorInvalidArgumentsProvider.class)
    void constructor_invalidArguments_throwsException(
        String firstName, 
        String lastName, 
        String email, 
        String passwordHash, 
        Address address, 
        Class<Throwable> type
    ) {
        assertThrows(type, () -> new User(firstName, lastName, email, passwordHash, address));
    }


    @ParameterizedTest
    @ArgumentsSource(NullAndEmptyArgumentsProvider.class)
    void setFirstName_invalidFirstName_throwsException(String firstName, Class<Throwable> exception) {
        User user = new User("first", "last", "e@mail.com", "1234", new Address());

        assertThrows(exception, () -> user.setFirstName(firstName));
    }

    @Test
    void setFirstName_nonBlankName_setsFirstName() {
        User user = new User("first", "last", "e@mail.com", "1234", new Address());

        user.setFirstName("yes");
        
        assertEquals("yes", user.getFirstName());
    }

    @ParameterizedTest
    @ArgumentsSource(NullAndEmptyArgumentsProvider.class)
    void setLastName_invalidLastName_throwsException(String firstName, Class<Throwable> exception) {
        User user = new User("first", "last", "e@mail.com", "1234", new Address());

        assertThrows(exception, () -> user.setLastName(firstName));
    }

    @Test
    void setLastName_nonBlankName_setsLastName() {
        User user = new User("first", "last", "e@mail.com", "1234", new Address());

        user.setLastName("newname");
        
        assertEquals("newname", user.getLastName());
    }

    @ParameterizedTest
    @ArgumentsSource(NullAndEmptyArgumentsProvider.class)
    void setEmail_invalidEmail_throwsException(String firstName, Class<Throwable> exception) {
        User user = new User("first", "last", "e@mail.com", "1234", new Address());

        assertThrows(exception, () -> user.setFirstName(firstName));
    }

    @Test
    void setEmail_nonBlankEmail_setsEmail() {
        User user = new User("first", "last", "e@mail.com", "1234", new Address());

        user.setLastName("e@xample.com");
        
        assertEquals("e@xample.com", user.getLastName());
    }


    @ParameterizedTest
    @ArgumentsSource(NullAndEmptyArgumentsProvider.class)
    void setPasswordHash_invalidPasswordHash_throwsException(String firstName, Class<Throwable> exception) {
        User user = new User("first", "last", "e@mail.com", "1234", new Address());

        assertThrows(exception, () -> user.setPasswordHash(firstName));
    }

    @Test
    void setPasswordHash_nonBlankHash_setsPasswordHash() {
        User user = new User("first", "last", "e@mail.com", "1234", new Address());

        user.setPasswordHash("farey%G%AEH%AR");
        
        assertEquals("farey%G%AEH%AR", user.getPasswordHash());
    }

    @Test
    void setAddress_nullAddress_throwsException() {
        User user = new User("first", "last", "e@mail.com", "1234", new Address());

        assertThrows(NullPointerException.class, () -> user.setAddress(null));
    }

    @Test
    void setAddress_nonNullAddress_setsAddress() {
        Address address = new Address();
        User user = new User("first", "last", "e@mail.com", "1234", new Address());

        user.setAddress(address);

        assertSame(address, user.getAddress());
    }

    static class NullAndEmptyArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
            return Stream.of(
                Arguments.of((String)null, NullPointerException.class),
                Arguments.of("", IllegalArgumentException.class),
                Arguments.of("  ", IllegalArgumentException.class)
            );
        }
    }

    static class ConstructorInvalidArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext arg0) throws Exception {
            return Stream.of(
                Arguments.of(null, "nameson", "na@example.com", "5$£þGR", new Address(), NullPointerException.class),
                Arguments.of("name", null, "na@example.com", "5$£þGR", new Address(), NullPointerException.class),
                Arguments.of("name", "nameson", null, "5$£þGR", new Address(), NullPointerException.class),
                Arguments.of("name", "nameson", "na@example.com", null, new Address(), NullPointerException.class),
                Arguments.of("name", "nameson", "na@example.com", "5$£þGR", null, NullPointerException.class),

                Arguments.of("  ", "nameson", "na@example.com", "5$£þGR", new Address(), IllegalArgumentException.class),
                Arguments.of("name", "  ", "na@example.com", "5$£þGR", new Address(), IllegalArgumentException.class),
                Arguments.of("name", "nameson", "  ", "5$£þGR", new Address(), IllegalArgumentException.class),
                Arguments.of("name", "nameson", "na@example.com", "  ", new Address(), IllegalArgumentException.class)
            );
        }
    }
}
