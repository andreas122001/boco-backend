package edu.ntnu.idatt2106.bocobackend.repository;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Instant;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import edu.ntnu.idatt2106.bocobackend.model.Address;
import edu.ntnu.idatt2106.bocobackend.model.Chat;
import edu.ntnu.idatt2106.bocobackend.model.ChatPrimaryKey;
import edu.ntnu.idatt2106.bocobackend.model.ChatMessage;
import edu.ntnu.idatt2106.bocobackend.model.Item;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.util.ItemCategory;
import edu.ntnu.idatt2106.bocobackend.util.PriceUnit;


@SpringBootTest
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class ChatMessageRepositoryTest {

    @Autowired ChatMessageRepository chatMessageRepository;
    @Autowired ChatRepository chatRepository;
    @Autowired UserRepository userRepository;
    @Autowired ItemRepository itemRepository;
    @Autowired ItemImageRepository itemImageRepository;

    private User user;
    private User initiator;

    private Chat createChat() {

        this.initiator = new User("Goerge", "Jefferson", "etphone@home.com", "4321", new Address("Earth", "1111"));
        this.user = new User("Name", "Nameson", "hello@email.com", "93tjgag%G%$$$$", new Address("Grove street", "7040"));
        userRepository.save(initiator);
        userRepository.save(user);

        Item item = new Item("IT developer", 15000000, PriceUnit.DAY, ItemCategory.OTHER, Instant.now().minusSeconds(150), Instant.now().plusSeconds(150), "Cheap dev available for hire", user);
        itemRepository.save(item);

        return new Chat(new ChatPrimaryKey(item, initiator));
    }

    @Test
    public void save_saveNewChatMessage_doesNotThrow() {
        Chat chat = createChat();
        chat = this.chatRepository.save(chat);
        ChatMessage message = new ChatMessage(Instant.now(), "Yo, my first message!", chat, initiator);
        
        assertDoesNotThrow(()-> this.chatMessageRepository.save(message));
    }

    @Test
    public void findById_getExistingChatMessage_doesNotThrow() {
        Chat chat = createChat();
        this.chatRepository.save(chat);
        
        ChatMessage message = new ChatMessage(Instant.now(), "Yo, my first message!", chat, initiator);
        message = this.chatMessageRepository.save(message);
        long messageId = message.getId();

        assertDoesNotThrow(()-> this.chatMessageRepository.findById(messageId));
    }

    @Test
    public void findById_getExistingChatMessage_returnsChatMessage() {
        Chat chat = createChat();
        this.chatRepository.save(chat);
        
        ChatMessage message = new ChatMessage(Instant.now(), "Yo, my first message!", chat, this.initiator);
        message = this.chatMessageRepository.save(message);

        assertEquals(message.getId(), this.chatMessageRepository.findById(message.getId()).get().getId());
    }

    @Test
    public void getAllMessagesByChat_existingChat_returnsMessages() {
        Chat chat = createChat();
        chat = this.chatRepository.save(chat);
        assertEquals(0, this.chatMessageRepository.getAllMessagesByChat(chat).size());
        
        ChatMessage message1 = new ChatMessage(Instant.now(), "Yo, my first message!", chat, this.initiator);
        message1 = this.chatMessageRepository.save(message1);

        ChatMessage message2 = new ChatMessage(Instant.now(), "How cool! Would you like to borrow my IT developer?", chat, this.user);
        message2 = this.chatMessageRepository.save(message2);

        ChatMessage message3 = new ChatMessage(Instant.now(), "No thank you, I just like sending messages. Have a nice day!", chat, this.initiator);
        message3 = this.chatMessageRepository.save(message3);

        assertEquals(3, this.chatMessageRepository.getAllMessagesByChat(chat).size());
    }
    
}