package edu.ntnu.idatt2106.bocobackend.repository;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import edu.ntnu.idatt2106.bocobackend.model.Address;
import edu.ntnu.idatt2106.bocobackend.model.Chat;
import edu.ntnu.idatt2106.bocobackend.model.ChatPrimaryKey;
import edu.ntnu.idatt2106.bocobackend.model.Item;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.util.ItemCategory;
import edu.ntnu.idatt2106.bocobackend.util.PriceUnit;

@SpringBootTest
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class ChatRepositoryTest {

    @Autowired ChatRepository chatRepository;
    @Autowired UserRepository userRepository;
    @Autowired ItemRepository itemRepository;
    @Autowired ItemImageRepository itemImageRepository;

    private ChatPrimaryKey createChatPK() {

        User initiator = new User("Goerge", "Jefferson", "etphone@home.com", "4321", new Address("Earth", "1111"));
        User user = new User("Name", "Nameson", "hello@email.com", "93tjgag%G%$$$$", new Address("Grove street", "7040"));
        
        userRepository.save(initiator);
        userRepository.save(user);

        Item item = new Item("IT developer", 150, PriceUnit.DAY, ItemCategory.OTHER, Instant.now().minusSeconds(150), Instant.now().plusSeconds(150), "Cheap dev available for hire", user);
        itemRepository.save(item);

        return new ChatPrimaryKey(item, initiator);
    }

    @Test
    public void save_saveNewChat_doesNotThrow() {
        Chat chat = new Chat(createChatPK());
        this.chatRepository.save(chat);
    }

    @Test
    public void findById_getExistingChat_returnsChat() {
        Chat chat = new Chat(createChatPK());
        this.chatRepository.save(chat);

        assertTrue(chat.getPrimaryKey().equals(this.chatRepository.findById(chat.getPrimaryKey()).get().getPrimaryKey()));
    }

    @Test
    public void findById_getExistingChat_doesNotThrow() {
        Chat chat = new Chat(createChatPK());
        this.chatRepository.save(chat);

        assertDoesNotThrow(()-> this.chatRepository.findById(chat.getPrimaryKey()));
    }

    @Test
    public void deleteById_deleteChat_doesNotThrow() {
        Chat chat = new Chat(createChatPK());
        ChatPrimaryKey chatId = this.chatRepository.save(chat).getPrimaryKey();
        
        this.chatRepository.deleteById(chatId);
    }

    @Test
    public void findByUser_returnsUserChats() {
        User initiator = new User("Goerge", "Jefferson", "etphone@home.com", "4321", new Address("Earth", "1111"));
        User user = new User("Name", "Nameson", "hello@email.com", "93tjgag%G%$$$$", new Address("Grove street", "7040"));
        
        userRepository.save(initiator);
        userRepository.save(user);

        Item item = new Item("IT developer", 150, PriceUnit.DAY, ItemCategory.OTHER, Instant.now().minusSeconds(150), Instant.now().plusSeconds(150), "Cheap dev available for hire", user);
        itemRepository.save(item);

        Chat chat = new Chat(new ChatPrimaryKey(item, initiator));
        this.chatRepository.save(chat);

        assertEquals(1, this.chatRepository.findByUser(user).size());
        assertEquals(1, this.chatRepository.findByUser(initiator).size());
    }
    
}