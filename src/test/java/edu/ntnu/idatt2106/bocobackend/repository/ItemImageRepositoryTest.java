package edu.ntnu.idatt2106.bocobackend.repository;

import java.time.Instant;

import edu.ntnu.idatt2106.bocobackend.util.ItemCategory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import edu.ntnu.idatt2106.bocobackend.model.Address;
import edu.ntnu.idatt2106.bocobackend.model.Item;
import edu.ntnu.idatt2106.bocobackend.model.ItemImage;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.util.PriceUnit;

@DataJpaTest
public class ItemImageRepositoryTest {

    @Autowired
    private ItemImageRepository itemImageRepository;

    @Test
    void save_saveItemImage_doseNotThrow(){
        byte[] data = { 1, 2, 3, 4 };
       
        User user = new User("man", "dem", "post@mail.com", "-", new Address("UK street", "4200"));
        Item item = new Item("baddie", 10, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now(), Instant.parse("2025-08-18T17:19:28.115Z"), " ", user);
        ItemImage image = new ItemImage(" qwer", data, "image/png", item);
        
        this.itemImageRepository.save(image);
    }
}
