package edu.ntnu.idatt2106.bocobackend.repository;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Instant;
import java.util.stream.Stream;

import edu.ntnu.idatt2106.bocobackend.util.ItemCategory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import edu.ntnu.idatt2106.bocobackend.model.Address;
import edu.ntnu.idatt2106.bocobackend.model.Item;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.util.PriceUnit;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class ItemRepositoryTest {
    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private UserRepository userRepository;

    User user;
    Item item;

    @BeforeEach
    public void initialize(){
        user = new User("markus", "johanne", "los@r.com", "-", new Address("nord", "6666"));
        user = this.userRepository.save(this.user);
    }
    
    @Test
    public void save_saveNewItem_doesNotThrow() {
        this.item = new Item("snus", 100, PriceUnit.DAY, ItemCategory.GARDEN,Instant.now(),Instant.now().plusSeconds(1),"lite keeg. 06 under leppa", this.user);
        
        assertDoesNotThrow(() -> this.itemRepository.save(item));
    }
    
    @Test
    public void findById_existingItem_returnsItem(){
        this.item = new Item("snus", 100, PriceUnit.DAY, ItemCategory.GARDEN,Instant.now(),Instant.now().plusSeconds(1),"lite keeg. 06 under leppa", this.user);
        this.item = this.itemRepository.save(this.item);
        
        Item foundItem = this.itemRepository.findById(item.getId()).get();

        assertEquals(this.item.getId(), foundItem.getId());
    }

    @Test
    public void delete_deleteExistingItem_deletesItem(){
        this.item = new Item("snus", 100, PriceUnit.DAY, ItemCategory.GARDEN,Instant.now(),Instant.now().plusSeconds(1),"lite keeg. 06 under leppa", this.user);
        this.item = this.itemRepository.save(this.item);
        
        this.itemRepository.delete(this.item);
        assertEquals(0, this.itemRepository.count());
    }

    @Test
    public void deleteItemById_deleteExistingItem_doesNotThrow(){
        this.item = new Item("snus", 100, PriceUnit.DAY, ItemCategory.GARDEN,Instant.now(),Instant.now().plusSeconds(1),"lite keeg. 06 under leppa", this.user);
        this.item = this.itemRepository.save(this.item);
        
        this.itemRepository.deleteById(this.item.getId());
        assertEquals(0, this.itemRepository.count());
    }

    @ParameterizedTest
    @ArgumentsSource(EmailSearchCategoryArgumentsProvider.class)
    public void findItems_findsItem(
        String search,
        String email,
        ItemCategory itemCategory
    ) {
        this.item = new Item("Snus", 100, PriceUnit.DAY, ItemCategory.GARDEN, Instant.now(), Instant.now().plusSeconds(1), "Lite keeg. 06 under leppa", this.user);
        this.item = this.itemRepository.save(this.item);

        assertEquals(
            1, 
            this.itemRepository.findItems(
                new ItemQueryParams().search(search).email(email).category(itemCategory)
            )
            .getContent().size()
        );
    }

    static class EmailSearchCategoryArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext arg0) {
            return Stream.of(
                Arguments.of(null, null, null),
                Arguments.of("snus", null, null),
                Arguments.of("snus", "los@r.com", null),
                Arguments.of("snus", "los@r.com", ItemCategory.GARDEN),
                Arguments.of(null, "los@r.com", null),
                Arguments.of(null, "los@r.com", ItemCategory.GARDEN),
                Arguments.of(null, null, ItemCategory.GARDEN),
                Arguments.of("snus", null, ItemCategory.GARDEN)
            );
        }
    }

}