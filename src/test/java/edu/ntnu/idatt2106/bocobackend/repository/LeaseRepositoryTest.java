package edu.ntnu.idatt2106.bocobackend.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import edu.ntnu.idatt2106.bocobackend.util.ItemCategory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import edu.ntnu.idatt2106.bocobackend.model.Address;
import edu.ntnu.idatt2106.bocobackend.model.Item;
import edu.ntnu.idatt2106.bocobackend.model.Lease;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.util.PriceUnit;

@SpringBootTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class LeaseRepositoryTest {

    @Autowired
    private LeaseRepository leaseRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ItemRepository itemRepository;

    User lessee1;
    User lessee2;

    User lessor1;
    User lessor2;

    Item item1;
    Item item2;

    @BeforeEach
    void setUp() {
        lessee1 = userRepository.save(new User(
            "John", 
            "Doe",
            "e@xample.com",
            "%¤#", 
            new Address("Foo 1", "1337")
        ));

        lessee2 = userRepository.save(new User(
            "Johnny", 
            "Bravo",
            "e@mail.com",
            "//&%", 
            new Address("Foo 2", "1337")
        ));

        lessor1 = userRepository.save(new User(
            "Jane", 
            "Doe",
            "ex@xample.com",
            "@&(", 
            new Address("Bar 1", "1337")
        ));

        lessor2 = userRepository.save(new User(
            "Janine", 
            "Dough",
            "em@ail.com",
            "(¤%&/", 
            new Address("Bar 2", "1337")
        ));

        item1 = itemRepository.save(new Item(
            "Fishing pole",
            420,
            PriceUnit.HOUR,
            ItemCategory.GARDEN,
            Instant.now().minusSeconds(100),
            Instant.now().plusSeconds(100),
            "A cool pole",
            lessor1
        ));

        item2 = itemRepository.save(new Item(
            "Used toothpick",
            999,
            PriceUnit.HOUR,
            ItemCategory.GARDEN,
            Instant.now().minusSeconds(200),
            Instant.now().plusSeconds(200),
            "A delicious stick",
            lessor2
        ));
    }

    Lease createDummyLease1() {
        return new Lease(
            lessee1,
            item1,
            Instant.now().minusSeconds(50),
            Instant.now().plusSeconds(50)
        );
    }

    Lease createDummyLease2() {
        return new Lease(
            lessee2,
            item2,
            Instant.now().minusSeconds(150),
            Instant.now().plusSeconds(150)
        );
    }

    @Test
    void save_savingLease_doesNotThrow() {
        leaseRepository.save(createDummyLease1());
    }

    @Test
    void findById_findExistingLease_returnsLease() {
        Lease lease = createDummyLease1();
        leaseRepository.save(lease);

        assertEquals(lease.getId(), leaseRepository.findById(lease.getId()).get().getId());
    }

    @Test
    void getLeasesByUser_findExistingLeases_returnsLeasesByUser() {
        Lease lease1 = createDummyLease1();
        Lease lease2 = createDummyLease2();
        leaseRepository.save(lease1);
        leaseRepository.save(lease2);
        assertEquals(2, this.leaseRepository.count());

        List<Lease> fetchedLeases = this.leaseRepository.getLeasesByUser(lease1.getLessee());

        assertEquals(1, fetchedLeases.size());
        assertEquals(lease1.getId(), fetchedLeases.get(0).getId());
    }

    @Test
    void deleteById_deleteExistingLease_deletesLease() {
        Lease lease = createDummyLease1();
        leaseRepository.save(lease);
        
        leaseRepository.deleteById(lease.getId());

        assertTrue(leaseRepository.findById(lease.getId()).isEmpty());
    }

    @Test
    void userDelete_deleteLessee_setsLesseeToNullAndKeepsLease() {
        Lease lease = createDummyLease1();
        lease = leaseRepository.save(lease);

        userRepository.deleteById(lease.getLessee().getId());

        Optional<Lease> optLease = leaseRepository.findById(lease.getId());

        assertFalse(userRepository.existsById(lease.getLessee().getId()));
        assertTrue(optLease.isPresent());
        assertNull(optLease.get().getLessee());
    }

    @Test
    void itemDelete_deleteItem_deletesLease() {
        Lease lease = createDummyLease1();
        lease = leaseRepository.save(lease);

        itemRepository.deleteById(lease.getItem().getId());

        assertTrue(itemRepository.findById(lease.getItem().getId()).isEmpty());
        assertTrue(leaseRepository.findById(lease.getId()).isEmpty());
    }

    @Test
    void getLeasesByItem_exsistingLeases_returnsLeases(){
        Lease lease1 = createDummyLease1();
        Lease lease2 = createDummyLease2();

        lease1=leaseRepository.save(lease1);
        lease2 = leaseRepository.save(lease2);

        List<Lease>list =  leaseRepository.getLeasesByItem(lease1.getItem());
        assertEquals(1, list.size());
    }
    
}
