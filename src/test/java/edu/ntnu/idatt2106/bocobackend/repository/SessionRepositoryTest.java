package edu.ntnu.idatt2106.bocobackend.repository;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mockStatic;

import java.time.Instant;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.model.Address;
import edu.ntnu.idatt2106.bocobackend.model.Session;

@DataJpaTest
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class SessionRepositoryTest {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SessionRepository sessionRepository;

    private User user;

    @BeforeEach
    void createUser() {
        user = new User(
            "John", 
            "Doe",
            "e@xample.com",
            "%¤#", 
            new Address("Foo 1", "1337")
        );
        userRepository.save(user);
    }

    @AfterEach
    void deleteUser() {
        userRepository.delete(user);
    }
    
    @Test
    public void save_savingSession_doesNotThrow() {
        Session session = new Session(user);

        sessionRepository.save(session);
    }

    @Test
    public void findById_existingSession_returnsSession() {
        Session session = new Session(user);

        sessionRepository.save(session);
        assertTrue(sessionRepository.findById(session.getToken()).isPresent());
    }

    @Test
    public void deleteById_deleteUser_doesNotThrow() {
        Session session = new Session(user);

        sessionRepository.save(session);
        sessionRepository.deleteById(session.getToken());
        assertTrue(sessionRepository.findById(session.getToken()).isEmpty());
    }

    @Test
    public void deleteAllExpired_expiredSessionsExist_sessionsAreRemoved() {
        Instant expired = Instant.now().minus(Session.ABSOLUTE_EXPIRY_DURATION);
        Session session;
        try (MockedStatic<Instant> instantMock = mockStatic(Instant.class, Mockito.CALLS_REAL_METHODS)) {
            instantMock.when(Instant::now).thenReturn(expired);
            session = new Session(user);
        }
        

        sessionRepository.save(session);
        sessionRepository.deleteAllExpired();

        assertFalse(sessionRepository.existsById(session.getToken()));
    }
}
