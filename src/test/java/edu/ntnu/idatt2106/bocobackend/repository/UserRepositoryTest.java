package edu.ntnu.idatt2106.bocobackend.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.model.Address;

@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    
    @Test
    public void save_saveNewUser_DoesNotThrow() {
        User user = new User("John", "Doe", "em@ail.com", "%¤#/&%¤/", new Address("Grove street", "7040"));
        this.userRepository.save(user);
    }

    @Test
    public void findById_getExistingUser_doesNotThrow() {
        User user = new User("John", "Doe", "em@ail.com", "%¤#/&%¤/", new Address("Grove street", "7040"));
        long userId = this.userRepository.save(user).getId();
        this.userRepository.findById(userId);
    }

    @Test
    public void deleteById_deleteUser_doesNotThrow() {
        User user = new User("John", "Doe", "em@ail.com", "%¤#/&%¤/", new Address("Grove street", "7040"));
        long userId = this.userRepository.save(user).getId();
        this.userRepository.deleteById(userId);
    }

    @Test
    public void save_saveExistingUser_overridesUser() {
        String initialName = "Johnb";
        String fixedName = "John";

        User user = new User(initialName, "Doe", "em@ail.com", "%¤#/&%¤/", new Address("Grove street", "7040"));
        user = this.userRepository.save(user);
        
        user.setFirstName(fixedName);
        this.userRepository.save(user);

        String savedName = this.userRepository.findById(user.getId()).get().getFirstName();
        
        assertEquals(fixedName, savedName);
    }

}
