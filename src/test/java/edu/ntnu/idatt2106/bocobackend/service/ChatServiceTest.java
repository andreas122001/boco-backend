package edu.ntnu.idatt2106.bocobackend.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Instant;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

import edu.ntnu.idatt2106.bocobackend.dto.ChatDTO;
import edu.ntnu.idatt2106.bocobackend.dto.ChatMessageDTO;
import edu.ntnu.idatt2106.bocobackend.dto.ItemDTO;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.AddressMapperImpl;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.ChatMapperImpl;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.ChatMessageMapperImpl;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.ItemImageMapperImpl;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.ItemMapperImpl;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.UserMapperImpl;
import edu.ntnu.idatt2106.bocobackend.model.Address;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.model.exception.ChatAlreadyExsistsException;
import edu.ntnu.idatt2106.bocobackend.model.exception.ForbiddenException;
import edu.ntnu.idatt2106.bocobackend.repository.UserRepository;
import edu.ntnu.idatt2106.bocobackend.security.PasswordEncoderConfig;
import edu.ntnu.idatt2106.bocobackend.util.ItemCategory;
import edu.ntnu.idatt2106.bocobackend.util.PriceUnit;

@DataJpaTest
@Import({
    ChatMapperImpl.class, 
    ItemImageMapperImpl.class, 
    ItemMapperImpl.class, 
    UserMapperImpl.class,
    AddressMapperImpl.class,
    ChatMessageMapperImpl.class,
    ItemService.class,
    UserService.class,
    ChatService.class,
    PasswordEncoderConfig.class
})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ChatServiceTest {

    @Autowired private ChatService chatService;
    
    @Autowired private UserRepository userRepository;
    @Autowired private ItemService itemService;

    private User lessor;
    private User initiator;
    private ItemDTO itemDTO;

    @BeforeEach
    void initialize() {
        this.lessor = new User("John", "Doe", "em@ail.com", "MeaCulpaSemper", new Address("Grove street", "7040"));
        this.initiator = new User("Thomas", "Wayne", "not@alive.com", "Gotham4Life", new Address("Wayne Manor", "6666"));
        this.itemDTO = new ItemDTO(1L, "Pagani Huayra", 99999999, PriceUnit.HOUR, ItemCategory.SPORTS, Instant.now().minusSeconds(50), Instant.now().plusSeconds(50), "Please drive carefully:--/", "em@ail.com", List.of());

        this.lessor = this.userRepository.save(this.lessor);
        this.initiator = this.userRepository.save(this.initiator);
        this.itemDTO = this.itemService.create(this.itemDTO, this.lessor);
    }

    @Test
    public void createChat_newChat_doesNotThrow() {
        ChatDTO chatDTO = new ChatDTO(this.itemDTO.getId(), this.itemDTO.getUserEmail(), this.initiator.getEmail());
        assertDoesNotThrow(()-> this.chatService.createChat(chatDTO, this.initiator));
    }

    @Test
    public void createChat_existingChat_throwsException() {
        ChatDTO chatDTO = new ChatDTO(this.itemDTO.getId(), this.itemDTO.getUserEmail(), this.initiator.getEmail());
        chatDTO = this.chatService.createChat(chatDTO, this.initiator);

        AtomicReference<ChatDTO> reference = new AtomicReference<>(chatDTO);
        assertThrows(
            ChatAlreadyExsistsException.class, 
            () -> this.chatService.createChat(reference.get(), this.initiator)
        );
    }

    @Test
    public void getChat_getExistingChat_returnsChat() {
        ChatDTO chatDTO = new ChatDTO(this.itemDTO.getId(), this.itemDTO.getUserEmail(), this.initiator.getEmail());
        chatDTO = this.chatService.createChat(chatDTO, this.initiator);

        assertEquals(chatDTO.getUuid(), this.chatService.getChat(this.itemDTO.getId(), this.initiator).getUuid());
    }

    @Test 
    public void getChats_existingChats_returnsChats() {
        assertEquals(0, this.chatService.getChats(this.initiator).size());

        ChatDTO chatDTO = new ChatDTO(this.itemDTO.getId(), this.itemDTO.getUserEmail(), this.initiator.getEmail());
        chatDTO = this.chatService.createChat(chatDTO, this.initiator);

        assertEquals(1, this.chatService.getChats(this.initiator).size());
        assertEquals(chatDTO.getUuid(), this.chatService.getChats(this.initiator).get(0).getUuid());
    }

    @Test 
    public void getChats_nonExistingChats_doesNotThrow() {
        assertDoesNotThrow(() -> this.chatService.getChats(this.initiator));
    }

    @Test 
    public void createChatMessage_existingChatAuthorizedUser_doesNotThrow() {
        ChatDTO chatDTO = new ChatDTO(this.itemDTO.getId(), this.itemDTO.getUserEmail(), this.initiator.getEmail());
        chatDTO = this.chatService.createChat(chatDTO, this.initiator);

        ChatMessageDTO chatMessageDTO = new ChatMessageDTO(Instant.now().plusSeconds(100), "Yo, issa message!", this.initiator.getEmail());
        chatMessageDTO = this.chatService.createChatMessage(String.valueOf(chatDTO.getUuid()), chatMessageDTO, this.initiator);
    }

    @Test 
    public void createChatMessage_existingChatUnauthorizedUser_throwsException() {
        ChatDTO chatDTO = new ChatDTO(this.itemDTO.getId(), this.itemDTO.getUserEmail(), this.initiator.getEmail());
        chatDTO = this.chatService.createChat(chatDTO, this.initiator);

        ChatMessageDTO chatMessageDTO = new ChatMessageDTO(Instant.now().plusSeconds(100), "Yo, issa message!", this.initiator.getEmail());
        chatMessageDTO = this.chatService.createChatMessage(String.valueOf(chatDTO.getUuid()), chatMessageDTO, this.initiator);

        User user = new User("Aqua", "Man", "water@living.com", "Aquavit", new Address("Atlantis, Great Ocean", "7040"));
        ChatMessageDTO chatMessageDTO2 = new ChatMessageDTO(Instant.now().plusSeconds(100), "Hydrohomies gang!", user.getEmail());
        
        UUID uuid = chatDTO.getUuid();
        assertThrows(
            ForbiddenException.class, 
            () -> this.chatService.createChatMessage(String.valueOf(uuid), chatMessageDTO2, user)
        );
    }

    @Test
    public void getMessages_existingChatAuthorizedUser_returnsMessages() {
        ChatDTO chatDTO = new ChatDTO(this.itemDTO.getId(), this.itemDTO.getUserEmail(), this.initiator.getEmail());
        chatDTO = this.chatService.createChat(chatDTO, this.initiator);
        assertEquals(0, this.chatService.getMessages(String.valueOf(chatDTO.getUuid()), this.initiator).size());

        ChatMessageDTO chatMessageDTO = new ChatMessageDTO(Instant.now().plusSeconds(100), "Yo, issa message!", this.initiator.getEmail());
        chatMessageDTO = this.chatService.createChatMessage(String.valueOf(chatDTO.getUuid()), chatMessageDTO, this.initiator);

        assertEquals(1, this.chatService.getMessages(String.valueOf(chatDTO.getUuid()), this.initiator).size());
    }

    @Test
    public void getMessages_existingChatUnauthorizedUser_throwsException() {
        ChatDTO chatDTO = new ChatDTO(this.itemDTO.getId(), this.itemDTO.getUserEmail(), this.initiator.getEmail());
        chatDTO = this.chatService.createChat(chatDTO, this.initiator);

        User user = new User("Aqua", "Man", "water@living.com", "Aquavit", new Address("Atlantis, Great Ocean", "7040"));

        UUID uuid = chatDTO.getUuid();
        assertThrows(
            ForbiddenException.class, 
            () -> this.chatService.getMessages(String.valueOf(uuid), user).size()
        );
    }

}