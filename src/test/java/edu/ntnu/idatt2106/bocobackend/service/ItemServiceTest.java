package edu.ntnu.idatt2106.bocobackend.service;

import edu.ntnu.idatt2106.bocobackend.dto.AddressDTO;
import edu.ntnu.idatt2106.bocobackend.dto.ItemDTO;
import edu.ntnu.idatt2106.bocobackend.dto.ItemImageDTO;
import edu.ntnu.idatt2106.bocobackend.dto.ItemsPageDTO;
import edu.ntnu.idatt2106.bocobackend.dto.UserDTO;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.AddressMapperImpl;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.ItemImageMapper;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.ItemImageMapperImpl;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.ItemMapper;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.ItemMapperImpl;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.UserMapper;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.UserMapperImpl;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.model.exception.ForbiddenException;
import edu.ntnu.idatt2106.bocobackend.model.exception.ImageDeletionException;
import edu.ntnu.idatt2106.bocobackend.model.exception.ItemImageNotFoundException;
import edu.ntnu.idatt2106.bocobackend.model.exception.ItemNotFoundException;
import edu.ntnu.idatt2106.bocobackend.repository.ItemImageRepository;
import edu.ntnu.idatt2106.bocobackend.repository.ItemRepository;
import edu.ntnu.idatt2106.bocobackend.security.PasswordEncoderConfig;
import edu.ntnu.idatt2106.bocobackend.util.ItemCategory;
import edu.ntnu.idatt2106.bocobackend.util.PaginationConstants;
import edu.ntnu.idatt2106.bocobackend.util.PriceUnit;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;

import java.time.Instant;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Import({
    PasswordEncoderConfig.class,
    AddressMapperImpl.class,
    UserMapperImpl.class,
    ItemImageMapperImpl.class,
    ItemMapperImpl.class,
    UserService.class,
    ItemService.class,
})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ItemServiceTest {
    @Autowired private TestEntityManager entityManager;
    @Autowired private ItemRepository itemRepository;
    @Autowired private ItemImageRepository itemImageRepository;
    @Autowired private ItemImageMapper itemImageMapper;
    @Autowired private ItemMapper itemMapper;
    @Autowired private UserService userService;
    @Autowired private ItemService itemService;
    @Autowired private UserMapper userMapper;

    private UserDTO lessor = new UserDTO("John", "Doe", "em@ail.com", "MeaCulpaSemper", new AddressDTO("Grove street", "7040"));
    private UserDTO lessor2 = new UserDTO("Thomas", "Wayne", "not@alive.com", "Gotham4Life", new AddressDTO("Wayne Manor", "6666"));

    @BeforeEach
    void initialize() {
        this.itemService = new ItemService(
            this.itemRepository, 
            this.itemImageRepository, 
            this.itemImageMapper, 
            this.itemMapper
        );

        this.lessor = this.userMapper.toDTO(this.userService.create(this.lessor));
        this.lessor2 = this.userMapper.toDTO(this.userService.create(this.lessor2));
    }

    private ItemDTO createItemDTO() {
        return new ItemDTO(
            1L,
            "3 slices of pizza",
            45,
            PriceUnit.DAY,
            ItemCategory.OTHER,
            Instant.now().minusSeconds(50),
            Instant.now().plusSeconds(50),
            "3 slices for borrowing only!",
            "em@ail.com",
            List.of()
        );
    }

    private ItemDTO createItemDTO2() {
        return new ItemDTO(
            2L,
            "Pagani Huayra",
            99999999,
            PriceUnit.HOUR,
            ItemCategory.SPORTS,
            Instant.now().minusSeconds(50),
            Instant.now().plusSeconds(50),
            "Please drive carefully:--/",
            "not@alive.com",
            List.of()
        );
    }

    @Test
    public void create_newItem_doesNotThrow() {
        ItemDTO dto = createItemDTO();
        User lessor = this.userService.getByEmail(dto.getUserEmail());
        
        assertDoesNotThrow(()-> this.itemService.create(dto, lessor));
    }

    @Test
    public void getItemById_existingItem_returnsItem() {
        ItemDTO dto = createItemDTO();
        User lessor = this.userService.getByEmail(dto.getUserEmail());
        dto = this.itemService.create(dto, lessor);
        
        assertEquals(dto.getId(), this.itemService.getItemById(dto.getId()).getId());
    }

    @Test
    public void getItemById_nonExistingItem_throwsException() {
        assertThrows(
            ItemNotFoundException.class, 
            () -> this.itemService.getItemById(1) 
        );
    }


    @Test
    public void getById_existingItem_returnsItem() {
        ItemDTO dto = createItemDTO();
        User lessor = this.userService.getByEmail(dto.getUserEmail());
        dto = this.itemService.create(dto, lessor);
        
        assertEquals(dto.getId(), this.itemService.getById(dto.getId()).getId());
    }

    @Test
    public void getById_nonExistingItem_throwsException() {
        assertThrows(
            ItemNotFoundException.class, 
            () -> this.itemService.getById(1) 
        );
    }

    @Test
    public void getItems_defaultParameters_returnsAllItems() {
        ItemDTO dto = createItemDTO();
        User lessor = this.userService.getByEmail(dto.getUserEmail());
        dto = this.itemService.create(dto, lessor);

        ItemDTO dto2 = createItemDTO2();
        User lessor2 = this.userService.getByEmail(dto2.getUserEmail());
        dto2 = this.itemService.create(dto2, lessor2);

        entityManager.flush();
        entityManager.clear();

        ItemsPageDTO foundItems = this.itemService.getItems(
            0,
            10,
            "id",
            "asc",
            null,
            null,
            null
        );

        assertEquals(
            2, 
            foundItems.getContent().size()
        );
    }

    @ParameterizedTest
    @ArgumentsSource(EmailSearchCategoryArgumentsProvider.class)
    public void findItems_findsItem(
        String search,
        String email,
        String itemCategory
    ) {
        ItemDTO dto = createItemDTO();
        User lessor = this.userService.getByEmail(dto.getUserEmail());
        dto = this.itemService.create(dto, lessor);

        ItemDTO dto2 = createItemDTO2();
        User lessor2 = this.userService.getByEmail(dto2.getUserEmail());
        dto2 = this.itemService.create(dto2, lessor2);

        entityManager.flush();
        entityManager.clear();

        ItemsPageDTO foundItems = this.itemService.getItems(
            Integer.parseInt(PaginationConstants.DEFAULT_PAGE_NUMBER),
            Integer.parseInt(PaginationConstants.DEFAULT_PAGE_SIZE),
            PaginationConstants.DEFAULT_SORT_BY,
            PaginationConstants.DEFAULT_SORT_DIRECTION,
            search,
            email,
            itemCategory
        );

        assertEquals(
            dto.getId(), 
            foundItems.getContent().get(0).getId()
        );
    }

    static class EmailSearchCategoryArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext arg0) {
            return Stream.of(
                Arguments.of(null, null, null),
                Arguments.of("pizza", null, null),
                Arguments.of("pizza", "em@ail.com", null),
                Arguments.of("pizza", "em@ail.com", "other"),
                Arguments.of(null, "em@ail.com", null),
                Arguments.of(null, "em@ail.com", "other"),
                Arguments.of(null, null, "other"),
                Arguments.of("pizza", null, "other")
            );
        }
    }

    @Test
    public void deleteById_existingItemAuthorizedUser_deletesItem() {
        ItemDTO dto = createItemDTO();
        User lessor = this.userService.getByEmail(dto.getUserEmail());
        dto = this.itemService.create(dto, lessor);

        this.itemService.deleteById(lessor, dto.getId());

        ItemsPageDTO foundItems = this.itemService.getItems(
            0,
            10,
            "id",
            "asc",
            "Pizza",
            dto.getUserEmail(),
            "other"
        );

        assertEquals(
            0, 
            foundItems.getContent().size()
        );
    }


    @Test
    public void deleteById_existingItemUnauthorizedUser_throwsException() {
        ItemDTO dto = createItemDTO();
        User lessor = this.userService.getByEmail(dto.getUserEmail());
        dto = this.itemService.create(dto, lessor);

        ItemDTO dto2 = createItemDTO2();
        User lessor2 = this.userService.getByEmail(dto2.getUserEmail());
        dto2 = this.itemService.create(dto2, lessor2);

        long id = dto.getId();
        assertThrows(
            ForbiddenException.class, 
            ()-> this.itemService.deleteById(lessor2, id)
        );
    }

    @Test
    public void deleteById_nonExistingItemAuthorizedUser_throwsException() {
        ItemDTO dto = createItemDTO();
        User lessor = this.userService.getByEmail(dto.getUserEmail());

        assertThrows(
            ItemNotFoundException.class, 
            ()-> this.itemService.deleteById(lessor, 1)
        );
    }

    @Test 
    public void update_authorizedUser_updatesItem() {
        ItemDTO dto = createItemDTO();
        User lessor = this.userService.getByEmail(dto.getUserEmail());
        dto = this.itemService.create(dto, lessor);

        String newTitle = "Issa new title!";
        int newPrice = 1337;
        PriceUnit newPriceUnit = PriceUnit.WEEK;
        ItemCategory newItemCategory = ItemCategory.ART;
        Instant newStartTime = Instant.now().minusSeconds(1337);
        Instant newEndTime = Instant.now().plusSeconds(1337);
        String newDescription = "Issa new description!";
        
        dto.setTitle(newTitle);
        dto.setPrice(newPrice);
        dto.setPriceUnit(newPriceUnit);
        dto.setCategory(newItemCategory);
        dto.setStartTime(newStartTime);
        dto.setEndTime(newEndTime);
        dto.setDescription(newDescription);

        dto = this.itemService.update(lessor, dto.getId(), dto);
        
        assertEquals(newTitle, dto.getTitle());
        assertEquals(newPrice, dto.getPrice());
        assertEquals(newPriceUnit, dto.getPriceUnit());
        assertEquals(newItemCategory, dto.getCategory());
        assertEquals(newStartTime, dto.getStartTime());
        assertEquals(newEndTime, dto.getEndTime());
        assertEquals(newDescription, dto.getDescription());
    }

    @Test 
    public void update_unauthorizedUser_throwsException() {
        ItemDTO dto = createItemDTO();
        User lessor = this.userService.getByEmail(dto.getUserEmail());
        dto = this.itemService.create(dto, lessor);

        ItemDTO dto2 = createItemDTO2();
        User lessor2 = this.userService.getByEmail(dto2.getUserEmail());
        dto2 = this.itemService.create(dto2, lessor2);

        String newTitle = "Issa new title!";
        int newPrice = 1337;
        PriceUnit newPriceUnit = PriceUnit.WEEK;
        ItemCategory newItemCategory = ItemCategory.ART;
        Instant newStartTime = Instant.now().minusSeconds(1337);
        Instant newEndTime = Instant.now().plusSeconds(1337);
        String newDescription = "Issa new description!";
        
        dto.setTitle(newTitle);
        dto.setPrice(newPrice);
        dto.setPriceUnit(newPriceUnit);
        dto.setCategory(newItemCategory);
        dto.setStartTime(newStartTime);
        dto.setEndTime(newEndTime);
        dto.setDescription(newDescription);
        
        final AtomicReference<ItemDTO> reference = new AtomicReference<>(dto);
        assertThrows(
            ForbiddenException.class, 
            () -> this.itemService.update(lessor2, reference.get().getId(), reference.get())
        );
    }

    @Test 
    public void addImageToItem_authorizedUser_addsImage() {
        ItemDTO dto = createItemDTO();
        User lessor = this.userService.getByEmail(dto.getUserEmail());
        dto = this.itemService.create(dto, lessor);

        ItemImageDTO itemImageDTO = new ItemImageDTO((long) 1, "Izza image", "data:image/png;base64,YWJj", dto.getId());
        itemImageDTO = this.itemService.addImageToItem(lessor, itemImageDTO, itemImageDTO.getItemId());
        entityManager.flush();
        entityManager.clear();
        
        dto = this.itemService.getById(dto.getId());

        assertEquals(itemImageDTO.getId(), dto.getImageIds().get(0));
    }

    @Test 
    public void addImageToItem_unauthorizedUser_throwsException() {
        ItemDTO dto = createItemDTO();
        User lessor = this.userService.getByEmail(dto.getUserEmail());
        dto = this.itemService.create(dto, lessor);

        ItemDTO dto2 = createItemDTO2();
        User lessor2 = this.userService.getByEmail(dto2.getUserEmail());
        dto2 = this.itemService.create(dto2, lessor2);

        ItemImageDTO itemImageDTO = new ItemImageDTO((long) 1, "Izza image", "data:image/png;base64,YWJj", dto.getId());
        
        assertThrows(
            ForbiddenException.class, 
            () -> this.itemService.addImageToItem(lessor2, itemImageDTO, itemImageDTO.getItemId())
        );
    }

    @Test 
    public void getImageById_exisitingImage_returnsImage() {
        ItemDTO dto = createItemDTO();
        User lessor = this.userService.getByEmail(dto.getUserEmail());
        dto = this.itemService.create(dto, lessor);

        ItemImageDTO itemImageDTO = new ItemImageDTO((long) 1, "Izza image", "data:image/png;base64,YWJj", dto.getId());
        itemImageDTO = this.itemService.addImageToItem(lessor, itemImageDTO, itemImageDTO.getItemId());
        entityManager.flush();
        entityManager.clear();

        dto = this.itemService.getById(dto.getId());

        assertEquals(
            itemImageDTO.getId(), 
            this.itemService.getImageById(dto.getImageIds().get(0)).getId()
        );
    }

    @Test 
    public void getImageById_nonExisitingImage_throwsException() {
        ItemDTO dto = createItemDTO();
        User lessor = this.userService.getByEmail(dto.getUserEmail());
        dto = this.itemService.create(dto, lessor);

        assertThrows(
            ItemImageNotFoundException.class, 
            () -> this.itemService.getImageById(1)
        );
    }

    @Test 
    public void removeImageFromItem_notLastImageOnItemAuthorizedUser_removesImage()  {
        ItemDTO dto = createItemDTO();
        User lessor = this.userService.getByEmail(dto.getUserEmail());
        dto = this.itemService.create(dto, lessor);

        ItemImageDTO itemImageDTO = new ItemImageDTO((long) 1, "Izza image", "data:image/png;base64,YWJj", dto.getId());
        ItemImageDTO itemImageDTO2 = new ItemImageDTO((long) 2, "Izza image", "data:image/png;base64,YWJj", dto.getId());

        itemImageDTO = this.itemService.addImageToItem(lessor, itemImageDTO, itemImageDTO.getItemId());
        itemImageDTO = this.itemService.addImageToItem(lessor, itemImageDTO2, itemImageDTO2.getItemId());
     
        this.itemService.removeImageFromItem(lessor, itemImageDTO.getId(), itemImageDTO.getItemId());
        entityManager.flush();
        entityManager.clear();
        
        dto = this.itemService.getById(dto.getId());
        assertEquals(1, dto.getImageIds().size());
    }

    @Test 
    public void removeImageFromItem_LastImageOnItemAuthorizedUser_throwsException() {
        ItemDTO dto = createItemDTO();
        User lessor = this.userService.getByEmail(dto.getUserEmail());
        dto = this.itemService.create(dto, lessor);

        ItemImageDTO itemImageDTO = new ItemImageDTO((long) 1, "Izza image", "data:image/png;base64,YWJj", dto.getId());

        ItemImageDTO itemImageDTO1 = this.itemService.addImageToItem(lessor, itemImageDTO, itemImageDTO.getItemId());
        entityManager.flush();
        entityManager.clear();

        dto = this.itemService.getById(dto.getId());
        assertEquals(1, dto.getImageIds().size());;
        long id = itemImageDTO.getId();
        
       assertThrows(ImageDeletionException.class,()-> this.itemService.removeImageFromItem(lessor, id, itemImageDTO1.getItemId())); 
       
    }

    @Test 
    public void removeImageFromItem_existingImageUnauthorizedUser_throwsException() {
        ItemDTO dto = createItemDTO();
        User lessor = this.userService.getByEmail(dto.getUserEmail());
        dto = this.itemService.create(dto, lessor);

        ItemDTO dto2 = createItemDTO2();
        User lessor2 = this.userService.getByEmail(dto2.getUserEmail());
        dto2 = this.itemService.create(dto2, lessor2);

        ItemImageDTO itemImageDTO = new ItemImageDTO((long) 1, "Izza image", "data:image/png;base64,YWJj", dto.getId());
        itemImageDTO = this.itemService.addImageToItem(lessor, itemImageDTO, itemImageDTO.getItemId());
        dto = this.itemService.getById(dto.getId());
        
        final AtomicReference<ItemImageDTO> reference = new AtomicReference<>(itemImageDTO);
        assertThrows(
            ForbiddenException.class, 
            () -> this.itemService.removeImageFromItem(lessor2, reference.get().getId(), reference.get().getItemId())
        );
    }

    @Test 
    public void removeImageFromItem_nonExistingImageAauthorizedUser_throwsException() {
        ItemDTO dto = createItemDTO();
        User lessor = this.userService.getByEmail(dto.getUserEmail());
        dto = this.itemService.create(dto, lessor);
        
        long id = dto.getId();
        assertThrows(
            ItemImageNotFoundException.class, 
            () -> this.itemService.removeImageFromItem(lessor, 1, id)
        );
    }
    
}
