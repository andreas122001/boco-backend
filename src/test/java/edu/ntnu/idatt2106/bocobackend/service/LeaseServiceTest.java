package edu.ntnu.idatt2106.bocobackend.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import edu.ntnu.idatt2106.bocobackend.util.ItemCategory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.mockito.Mock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import edu.ntnu.idatt2106.bocobackend.dto.AddressDTO;
import edu.ntnu.idatt2106.bocobackend.dto.ItemDTO;
import edu.ntnu.idatt2106.bocobackend.dto.LeaseDTO;
import edu.ntnu.idatt2106.bocobackend.dto.LeasePeriodDTO;
import edu.ntnu.idatt2106.bocobackend.dto.UserDTO;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.LeaseMapper;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.model.Username;
import edu.ntnu.idatt2106.bocobackend.model.exception.ForbiddenException;
import edu.ntnu.idatt2106.bocobackend.repository.LeaseRepository;
import edu.ntnu.idatt2106.bocobackend.util.Actor;
import edu.ntnu.idatt2106.bocobackend.util.PriceUnit;

@SpringBootTest
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class LeaseServiceTest {

    @Autowired
    private LeaseService leaseService;

    @Autowired
    private LeaseRepository leaseRepository;
    
    @Autowired
    private ItemService itemService;
    
    @Autowired
    private UserService userService;

    @Autowired
    LeaseMapper leaseMapper;

    private User lessor;

    private Actor lessorActor;

    @Mock
    private PasswordEncoder passwordEncoder;

    @BeforeEach 
    void initialize() {
        this.leaseService= new LeaseService(
            this.leaseRepository,
            this.itemService,
            this.userService,
            this.leaseMapper
        );

        UserDTO userDto = createUserDto();
        this.lessor = this.userService.create(userDto);
        this.lessorActor = new Actor(this.lessor);

        ItemDTO itemDto = createItemDto();
        itemService.create(itemDto, lessor);
    }

    private UserDTO createUserDto() {
        String password = "itRainsItPours";
        String passwordHash = "hash";
        when(passwordEncoder.encode(eq(password))).thenReturn(passwordHash);

        return new UserDTO("John", "Doe", "em@ail.com", password, new AddressDTO("Grove street", "7040"));
    }

    private ItemDTO createItemDto() {
        return new ItemDTO(
            1L,
            "3 slices of pizza",
            45,
            PriceUnit.DAY,
            ItemCategory.GARDEN,
            Instant.now().minusSeconds(50),
            Instant.now().plusSeconds(50),
            "3 slices for borrowing only!",
            "em@ail.com",
            List.of()
        );
        
    }

    private LeaseDTO createLeaseDto() {
        return new LeaseDTO(
            "deadbeef-0000-4000-0000-000000000000",
            "em@ail.com",
            1,
            Instant.parse("2021-08-18T17:19:28.115Z"),
            Instant.parse("2024-08-18T17:19:28.115Z")
        );
    }


    private boolean areEqual(LeaseDTO one, LeaseDTO other) {
        return one.getLesseeEmail().equals(other.getLesseeEmail()) &&
        one.getItemId() == other.getItemId() &&
        one.getStartTime().equals(other.getStartTime()) &&
        one.getEndTime().equals(other.getEndTime());
    }

    @Test
    public void create_newLease_doesNotThrow() {
        LeaseDTO leaseDto = createLeaseDto();
        
        assertDoesNotThrow(() -> this.leaseService.create(
            lessorActor,
            Username.SELF,
            leaseDto)
        );
    }
    

    @Test
    public void getById_existingLease_returnsLease() {
        LeaseDTO leaseDto = createLeaseDto();
        leaseDto = this.leaseService.create(lessorActor, Username.SELF, leaseDto);

        LeaseDTO leaseDtoDb = this.leaseService.getById(lessorActor, leaseDto.getId());

        assertTrue(this.areEqual(leaseDtoDb, leaseDto));
    }

    @Test
    public void getById_nonExistingLease_throwsException() {
        assertThrows(
            ForbiddenException.class,
            () -> this.leaseService.getById(lessorActor, UUID.randomUUID().toString())
        );
    }

    @Test
    public void itemIsLeasedInTimePeriod_timePeriodAvaliable_returnsFalse(){
       
        LeaseDTO lease1 = createLeaseDto();
        lease1.setStartTime(Instant.parse("2020-10-18T17:19:28.115Z"));
        lease1.setEndTime(Instant.parse("2023-10-18T17:19:28.115Z"));

        LeaseDTO lease2 = createLeaseDto();
        lease2.setStartTime(Instant.parse("2025-10-18T17:19:28.115Z"));
        lease2.setEndTime(Instant.parse("2030-10-18T17:19:28.115Z"));

        this.leaseService.create(lessorActor, Username.SELF, lease1);
        assertFalse( ()-> this.leaseService.itemIsLeasedInTimePeriod(lease2));
    }
    

    @Test
    public void itemIsLeasedInTimePeriod_endTimeUnvalid_returnsTrue(){
       
        LeaseDTO lease1 = createLeaseDto();
        lease1.setStartTime(Instant.parse("2020-10-18T17:19:28.115Z"));
        lease1.setEndTime(Instant.parse("2023-10-18T17:19:28.115Z"));

        LeaseDTO lease2 = createLeaseDto();
        lease2.setStartTime(Instant.parse("2019-10-18T17:19:28.115Z"));
        lease2.setEndTime(Instant.parse("2022-10-18T17:19:28.115Z"));

        this.leaseService.create(lessorActor, Username.SELF, lease1);
        assertTrue( ()-> this.leaseService.itemIsLeasedInTimePeriod(lease2));
    }

    @Test
    public void itemIsLeasedInTimePeriod_startTimeUnvalid_returnsTrue(){
       
        LeaseDTO lease1 = createLeaseDto();
        lease1.setStartTime(Instant.parse("2020-10-18T17:19:28.115Z"));
        lease1.setEndTime(Instant.parse("2023-10-18T17:19:28.115Z"));

        LeaseDTO lease2 = createLeaseDto();
        lease2.setStartTime(Instant.parse("2022-10-18T17:19:28.115Z"));
        lease2.setEndTime(Instant.parse("2024-10-18T17:19:28.115Z"));

        this.leaseService.create(lessorActor, Username.SELF, lease1);
        assertTrue( ()-> this.leaseService.itemIsLeasedInTimePeriod(lease2));
    }

    @Test
    public void itemIsLeasedInTimePeriod_StartimeBeforeStartime_EndtimeAfterEndtime_returnsTrue(){
       
        LeaseDTO lease1 = createLeaseDto();
        lease1.setStartTime(Instant.parse("2020-10-18T17:19:28.115Z"));
        lease1.setEndTime(Instant.parse("2021-10-18T17:19:28.115Z"));

        LeaseDTO lease2 = createLeaseDto();
        lease2.setStartTime(Instant.parse("2019-10-18T17:19:28.115Z"));
        lease2.setEndTime(Instant.parse("2022-10-18T17:19:28.115Z"));

        this.leaseService.create(lessorActor, Username.SELF, lease1);
        assertTrue( ()-> this.leaseService.itemIsLeasedInTimePeriod(lease2));
    }

    @Test
    public void itemIsLeasedInTimePeriod_StartimeAfterStartime_EndtimeBeforeEndtime_returnsTrue(){
       
        LeaseDTO lease1 = createLeaseDto();
        lease1.setStartTime(Instant.parse("2020-10-18T17:19:28.115Z"));
        lease1.setEndTime(Instant.parse("2023-10-18T17:19:28.115Z"));

        LeaseDTO lease2 = createLeaseDto();
        lease2.setStartTime(Instant.parse("2021-10-18T17:19:28.115Z"));
        lease2.setEndTime(Instant.parse("2022-10-18T17:19:28.115Z"));

        this.leaseService.create(lessorActor, Username.SELF, lease1);
        assertTrue( ()-> this.leaseService.itemIsLeasedInTimePeriod(lease2));
    }

    @Test
    public void getAllLeasesByItemID(){
        LeaseDTO lease1 = createLeaseDto();
        lease1.setStartTime(Instant.parse("2020-10-18T17:19:28.115Z"));
        lease1.setEndTime(Instant.parse("2023-10-18T17:19:28.115Z"));

        LeaseDTO lease2 = createLeaseDto();
        lease2.setStartTime(Instant.parse("2024-10-18T17:19:28.115Z"));
        lease2.setEndTime(Instant.parse("2026-10-18T17:19:28.115Z"));

        this.leaseService.create(lessorActor, Username.SELF, lease1);
        this.leaseService.create(lessorActor, Username.SELF, lease2);

        List<LeasePeriodDTO> list = leaseService.getAllLeasesByItemID(lease1.getItemId());
        assertEquals(lease1.getStartTime(), list.get(0).getStartTime());
        assertEquals(lease1.getEndTime(), list.get(0).getEndTime());
    }
    



    

}
