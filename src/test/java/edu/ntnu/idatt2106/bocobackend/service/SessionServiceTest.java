package edu.ntnu.idatt2106.bocobackend.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import org.apache.tomcat.util.codec.binary.Base64;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import edu.ntnu.idatt2106.bocobackend.dto.SessionDTO;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.SessionMapper;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.SessionMapperImpl;
import edu.ntnu.idatt2106.bocobackend.model.Address;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.model.UserCredentials;
import edu.ntnu.idatt2106.bocobackend.model.exception.InvalidCredentialsException;
import edu.ntnu.idatt2106.bocobackend.model.exception.SessionNotFoundException;
import edu.ntnu.idatt2106.bocobackend.repository.SessionRepository;
import edu.ntnu.idatt2106.bocobackend.repository.UserRepository;

@DataJpaTest
@Import(SessionMapperImpl.class)
@ExtendWith(MockitoExtension.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class SessionServiceTest {
    @Mock
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    SessionRepository sessionRepository;

    @Autowired
    SessionMapper sessionMapper;

    SessionService sessionService;
    
    User user;

    @BeforeEach
    public void setup() {
        sessionService =  new SessionService(userService, sessionRepository, sessionMapper);
        user = new User(
            "TestLover", 
            "64", 
            "testlover@example.com", 
            "&GFDSG&%¤", 
            new Address("Foobar 2", "1337")
        );
        userRepository.save(user);
    }

    @Test
    public void createSession_validCredentials_returnsSessionForTheUser() {
        when(userService.getByEmailAndPassword(anyString(), anyString())).thenReturn(user);
        UserCredentials credentials = new UserCredentials("testlover@example.com", "pass");

        SessionDTO session = sessionService.create(credentials);

        assertNotNull(session.getToken());
    }

    @Test
    public void createSession_invalidUsernameOrPassword_throwsInvalidCredentialsError() {
        when(userService.getByEmailAndPassword(anyString(), anyString())).thenThrow(InvalidCredentialsException.class);
        UserCredentials credentials = new UserCredentials("wrong@email.com", "ILoveTests!");

        assertThrows(InvalidCredentialsException.class, () -> sessionService.create(credentials));
    }

    @Test
    public void deleteSession_validToken_removesSession() {
        when(userService.getByEmailAndPassword(anyString(), anyString())).thenReturn(user);
        UserCredentials credentials = new UserCredentials("testlover@example.com", "pass");

        SessionDTO session = sessionService.create(credentials);
        String token = session.getToken();
        sessionService.delete(token);

        assertTrue(sessionRepository.findById(Base64.decodeBase64URLSafe(token)).isEmpty());
    }

    @Test
    public void deleteSession_invalidToken_throwsNotFoundException() {
        assertThrows(SessionNotFoundException.class, () -> sessionService.delete("thoken"));
    }
}
