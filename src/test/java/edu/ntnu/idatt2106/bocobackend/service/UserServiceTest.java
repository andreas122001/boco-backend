package edu.ntnu.idatt2106.bocobackend.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import edu.ntnu.idatt2106.bocobackend.model.Address;
import edu.ntnu.idatt2106.bocobackend.model.User;
import edu.ntnu.idatt2106.bocobackend.dto.AddressDTO;
import edu.ntnu.idatt2106.bocobackend.dto.ChangePasswordDTO;
import edu.ntnu.idatt2106.bocobackend.dto.UserDTO;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.AddressMapperImpl;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.UserMapper;
import edu.ntnu.idatt2106.bocobackend.dto.mapper.UserMapperImpl;
import edu.ntnu.idatt2106.bocobackend.model.exception.UserAlreadyExistsException;
import edu.ntnu.idatt2106.bocobackend.model.exception.IdenticalPasswordsException;
import edu.ntnu.idatt2106.bocobackend.model.exception.InvalidCredentialsException;
import edu.ntnu.idatt2106.bocobackend.model.exception.ForbiddenException;
import edu.ntnu.idatt2106.bocobackend.model.exception.UserNotFoundException;
import edu.ntnu.idatt2106.bocobackend.repository.UserRepository;
import edu.ntnu.idatt2106.bocobackend.util.Actor;

@DataJpaTest
@Import({
    AddressMapperImpl.class,
    UserMapperImpl.class
})
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class UserServiceTest {

    private UserService userService;
    
    @Autowired
    UserRepository userRepository;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Autowired
    UserMapper userMapper;

    @BeforeEach 
    void initialize() {
        this.userService = new UserService(this.userRepository, this.passwordEncoder, userMapper);
    }

    private UserDTO createUserDto() {
        String password = "itRainsItPours";
        String passwordHash = "hash";
        when(passwordEncoder.encode(eq(password))).thenReturn(passwordHash);

        return new UserDTO("John", "Doe", "em@ail.com", password, new AddressDTO("Grove street", "7040"));
    }

    @Test
    public void create_newUser_doesNotThrow() {
        UserDTO userDto = createUserDto();

        this.userService.create(userDto);
    }

    @Test
    public void create_existingUser_throwsException() {
        UserDTO userDto = createUserDto();
        this.userService.create(userDto);
        
        assertThrows(
            UserAlreadyExistsException.class,
            () -> this.userService.create(userDto)
        );
    }

    @Test
    public void getByEmail_existingUser_returnsUser() {
        UserDTO userDto = createUserDto();
        User user = this.userService.create(userDto);
        UserDTO userFromDb = this.userService.getProfileByEmail(user.getEmail());
        assertEquals(userFromDb.getEmail(), user.getEmail());
    }

    @Test
    public void getByEmail_nonExistingUser_throwsException() {
        User user = new User("John", "Doe", "em@ail.com", "%¤#/&%¤/", new Address("Grove street", "7040"));

        assertThrows(
            UserNotFoundException.class,
            () -> this.userService.getProfileByEmail(user.getEmail()));
    }

    @Test
    public void getByEmailAndPassword_validCredentials_returnsUser() {
        String password = "best password 1";
        String passwordHash = "hash";

        when(passwordEncoder.encode(eq(password))).thenReturn(passwordHash);
        when(passwordEncoder.matches(eq(password), eq(passwordHash))).thenReturn(true);

        UserDTO userDto = new UserDTO("John", "Doe", "em@ail.com", password, new AddressDTO("Grove street", "7040"));

        User user = this.userService.create(userDto);

        User userFromDb = this.userService.getByEmailAndPassword(user.getEmail(), password);

        assertEquals(userFromDb.getId(), user.getId());
    }

    @Test
    public void getByEmailAndPassword_invalidCredentials_throwsException() {
        String password = "best password 1";
        String passwordHash = "hash";

        when(passwordEncoder.encode(eq(password))).thenReturn(passwordHash);
        when(passwordEncoder.matches(eq(password), eq(passwordHash))).thenReturn(true);

        UserDTO userDto = new UserDTO("John", "Doe", "em@ail.com", password, new AddressDTO("Grove street", "7040"));

        User user = this.userService.create(userDto);

        assertThrows(
            InvalidCredentialsException.class,
            () -> this.userService.getByEmailAndPassword(user.getEmail(), "best password 2")
        ); 
    }

    @Test
    public void deleteById_existingUser_deletesUser() {
        String password = "itRainsItPours";
        String passwordHash = "hash";
        when(passwordEncoder.encode(eq(password))).thenReturn(passwordHash);

        UserDTO userDto = new UserDTO("John", "Doe", "em@ail.com", password, new AddressDTO("Grove street", "7040"));

        User user = this.userService.create(userDto);
        this.userService.deleteByUsername(new Actor(user), user.getEmail());
        assertDoesNotThrow(() -> this.userService.create(userDto));
    }

    @Test
    public void deleteByUsername_nonExistingUser_throwsException() {
        User user = new User("John", "Doe", "em@ail.com", "%¤#/&%¤/", new Address("Grove street", "7040"));

        assertThrows(
            ForbiddenException.class,
            () -> this.userService.deleteByUsername(new Actor(user), "foobar")
        );
    }

    @Test
    public void setPasswordByUsername_exsistingUser_setsPassword() {
        String oldPassword = "OldIsKing";
        String oldPasswordHash = "hash";
        String newPassword = "TheFutureIsNow";
        String newPasswordHash = "newHash";

        when(passwordEncoder.encode(eq(oldPassword))).thenReturn(oldPasswordHash);
        when(passwordEncoder.matches(eq(oldPassword), eq(oldPasswordHash))).thenReturn(true);

        UserDTO userDto = new UserDTO("John", "Doe", "em@ail.com", oldPassword, new AddressDTO("Grove street", "7040"));

        User user = this.userService.create(userDto);

        when(passwordEncoder.encode(eq(newPassword))).thenReturn(newPasswordHash);
        when(passwordEncoder.matches(eq(newPassword), eq(newPasswordHash))).thenReturn(true);
        this.userService.setPasswordByUsername(new Actor(user), user.getEmail(), new ChangePasswordDTO(oldPassword, newPassword));

        assertTrue(passwordEncoder.matches(newPassword, user.getPasswordHash()));
    }

    @Test
    public void setPasswordByUsername_invalidCredentials_throwsException() {
        String oldPassword = "OldIsKing";
        String oldPasswordHash = "hash";
        String newPassword = "TheFutureIsNow";
        String newPasswordHash = "newHash";

        when(passwordEncoder.encode(eq(oldPassword))).thenReturn(oldPasswordHash);
        when(passwordEncoder.matches(eq(oldPassword), eq(oldPasswordHash))).thenReturn(true);

        UserDTO userDto = new UserDTO("John", "Doe", "em@ail.com", oldPassword, new AddressDTO("Grove street", "7040"));

        User user = this.userService.create(userDto);

        when(passwordEncoder.encode(eq(newPassword))).thenReturn(newPasswordHash);
        when(passwordEncoder.matches(eq(newPassword), eq(newPasswordHash))).thenReturn(true);

        assertThrows(
            InvalidCredentialsException.class,
            () -> this.userService.setPasswordByUsername(
                new Actor(user), 
                user.getEmail(), 
                new ChangePasswordDTO("wrongPass", newPassword)
            )
        );
    }

    @Test
    public void setPasswordByUsername_identicalPasswords_throwsException() {
        String oldPassword = "OldIsKing";
        String oldPasswordHash = "hash";
        String newPassword = "TheFutureIsNow";
        String newPasswordHash = "newHash";

        when(passwordEncoder.encode(eq(oldPassword))).thenReturn(oldPasswordHash);
        when(passwordEncoder.matches(eq(oldPassword), eq(oldPasswordHash))).thenReturn(true);

        UserDTO userDto = new UserDTO("John", "Doe", "em@ail.com", oldPassword, new AddressDTO("Grove street", "7040"));

        User user = this.userService.create(userDto);

        when(passwordEncoder.encode(eq(newPassword))).thenReturn(newPasswordHash);
        when(passwordEncoder.matches(eq(newPassword), eq(newPasswordHash))).thenReturn(true);

        assertThrows(
            IdenticalPasswordsException.class,
            () -> this.userService.setPasswordByUsername(
                new Actor(user), 
                user.getEmail(), 
                new ChangePasswordDTO(oldPassword, "OldIsKing")
            )
        );
    }

    @Test
    public void updateUser_updatesUser() {
        UserDTO userDto = createUserDto();
        User user = this.userService.create(userDto);

        userDto.setFirstName("NewFirstName");
        userDto.setLastName("NewLastName");
        userDto.setEmail("new@email.com");
        userDto.setAddress(new AddressDTO("New Street", "1111"));

        this.userService.updateUserByUsername(new Actor(user), "em@ail.com", userDto);

        assertEquals("NewFirstName", user.getFirstName());
        assertEquals("NewLastName", user.getLastName());
        assertEquals("new@email.com", user.getEmail());
        assertEquals("New Street", user.getAddress().getStreet());
        assertEquals("1111", user.getAddress().getPostalCode());
    }
}
