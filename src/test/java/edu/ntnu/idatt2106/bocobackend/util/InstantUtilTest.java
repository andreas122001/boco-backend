package edu.ntnu.idatt2106.bocobackend.util;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Instant;

import org.junit.jupiter.api.Test;

public class InstantUtilTest {
    Instant startTime = Instant.parse("2017-10-20T16:55:30.00Z");
    Instant endTime = Instant.parse("2018-10-20T16:55:30.00Z");
    
        @Test
       void startTime_Notvalid_ChronologicalTimes(){
        
        assertThrows(IllegalArgumentException.class, () -> InstantUtil.startTimeChronological(endTime, startTime));

       }

       @Test
       void endTime_Notvalid_ChronologicalTimes(){
        
        assertThrows(IllegalArgumentException.class, () -> InstantUtil.endTimeChronological(endTime, startTime));

       }
       @Test
       void instantsEqual_notVaild_startCT(){
        assertThrows(IllegalArgumentException.class, () -> InstantUtil.endTimeChronological(startTime, startTime));
       }
       @Test
       void instantsEqual_notVaild_endCT(){
        assertThrows(IllegalArgumentException.class, () -> InstantUtil.endTimeChronological(endTime, endTime));
       }

       @Test
       void startTime_Valid_ChronologicalTimes(){
        Instant result = InstantUtil.startTimeChronological(startTime, endTime);
        assertSame(startTime, result);
       }
       
       @Test
       void endTime_Valid_ChronologicalTimes(){
        Instant result = InstantUtil.endTimeChronological(startTime, endTime);
        assertSame(endTime, result);
         
       }
    
}
