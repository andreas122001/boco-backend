package edu.ntnu.idatt2106.bocobackend.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class StringUtilTest {
    @Test
    void trimAndThrowIfNullOrBlank_nullString_throwsException() {
        assertThrows(NullPointerException.class, () -> StringUtil.trimAndThrowIfNullOrBlank(null));
    }

    @Test
    void trimAndThrowIfNullOrBlank_blankString_throwsException() {
        assertThrows(IllegalArgumentException.class, () -> StringUtil.trimAndThrowIfNullOrBlank("  "));
    }

    @Test
    void trimAndThrowIfNullOrBlank_paddedString_returnsTrimmedString() {
        String str = StringUtil.trimAndThrowIfNullOrBlank("  foobar  ");
        
        assertEquals("foobar", str);
    }
}
