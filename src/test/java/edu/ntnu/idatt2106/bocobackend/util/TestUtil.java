package edu.ntnu.idatt2106.bocobackend.util;


import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Util for tests.
 */
public class TestUtil {
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
